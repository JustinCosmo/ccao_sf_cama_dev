                                  *SPSS Regression.
				    *Hyde Park and South Multiple Regression 2012.


Get file='C:\Program Files\IBM\SPSS\statistics\19\1\regt70andregt76mergefcl3.sav'.
*select if (amount1>65000).
*select if (amount1<550000).
*select if (multi<1).
*select if sqftb<9000.

Compute year1=0.
If (amount1>0) year1=1900 + yr.
If (yr=11 and amount1>0) year1=2011.
If (yr=10 and amount1>0) year1=2010.
If (yr=9 and amount1>0)  year1=2009.
If (yr=8 and amount1>0)  year1=2008.
If (yr=7 and amount1>0)  year1=2007.   
If (yr=6 and amount1>0)  year1=2006. 
If (yr=5 and amount1>0)  year1=2005.
If (yr=4 and amount1>0)  year1=2004.
If (yr=3 and amount1>0)  year1=2003.   
If (yr=2 and amount1>0)  year1=2002.  
If (yr=1 and amount1>0)  year1=2001.   
If (yr=0 and amount1>0)  year1=2000.
*select if (year1>2006).

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	20021020030000
or pin=	20021060170000
or pin=	20021070100000
or pin=	20021070550000
or pin=	20021110150000
or pin=	20021160060000
or pin=	20023020190000
or pin=	20023070630000
or pin=	20023140130000
or pin=	20024000130000
or pin=	20024000440000
or pin=	20024040160000
or pin=	20031030210000
or pin=	20031110370000
or pin=	20031220100000
or pin=	20032010210000
or pin=	20032030040000
or pin=	20032130230000
or pin=	20032140260000
or pin=	20032190340000
or pin=	20032200080000
or pin=	20032240180000
or pin=	20033100220000
or pin=	20034000470000
or pin=	20034070170000
or pin=	20034080220000
or pin=	20034150010000
or pin=	20034160100000
or pin=	20101000400000
or pin=	20101030180000
or pin=	20102040370000
or pin=	20102070350000
or pin=	20102130330000
or pin=	20102190070000
or pin=	20103040060000
or pin=	20103050330000
or pin=	20103050340000
or pin=	20103070150000
or pin=	20103120060000
or pin=	20151070430000
or pin=	20151100040000
or pin=	20153010180000
or pin=	20153030290000
or pin=	20153090050000
or pin=	20111140280000
or pin=	20111140370000
or pin=	20111140380000
or pin=	20112060740000
or pin=	20114160320000
or pin=	20142010240000
or pin=	20154010140000
or pin=	20154030210000
or pin=	20154100100000
or pin=	20154150040000
or pin=	20154200200000
or pin=	20222090210000
or pin=	20222100060000
or pin=	20222140100000
or pin=	20222180250000
or pin=	20222190170000
or pin=	20222230100000
or pin=	20222280370000
or pin=	20222290130000
or pin=	20222290200000
or pin=	20222310300000
or pin=	20223030090000
or pin=	20223030290000
or pin=	20223090370000
or pin=	20223100380000
or pin=	20223110280000
or pin=	20223230290000
or pin=	20223260030000
or pin=	20223260050000
or pin=	20224010070000
or pin=	20224190140000
or pin=	20271020110000
or pin=	20271080120000
or pin=	20271080380000
or pin=	20271170280000
or pin=	20271230050000
or pin=	20272040030000
or pin=	20272060280000
or pin=	20272100050000
or pin=	20272290320000
or pin=	20272300180000
or pin=	20274010280000
or pin=	20274010330000
or pin=	20274040300000
or pin=	20274080140000
or pin=	20274120120000
or pin=	20274160060000
or pin=	20274310060000
or pin=	20143160220000
or pin=	20144080050000
or pin=	20231040090000
or pin=	20231050580000
or pin=	20231060480000
or pin=	20231100240000
or pin=	20231190070000
or pin=	20231200250000
or pin=	20231210350000
or pin=	20231270070000
or pin=	20232130400000
or pin=	20234060060000
or pin=	20234100360000
or pin=	20261040040000
or pin=	20261040270000
or pin=	20261150160000
or pin=	20261240050000
or pin=	20262050100000
or pin=	20262110130000
or pin=	20262200300000
or pin=	20262210350000
or pin=	20263040150000
or pin=	20263090090000
or pin=	20263160210000
or pin=	20263210050000
or pin=	20263220300000
or pin=	20251020090000
or pin=	20251080160000
or pin=	20251140030000
or pin=	20253090330000
or pin=	20253100070000
or pin=	20253170110000
or pin=	20253200200000
or pin=	20254100110000
or pin=	20254170370000
or pin=	20254220270000
or pin=	20351131160000
or pin=	20351241710000
or pin=	20352120140000
or pin=	20352190100000
or pin=	20354020470000
or pin=	20354090210000
or pin=	20354160360000
or pin=	20361020210000
or pin=	20361080350000
or pin=	20361160080000
or pin=	20362070120000
or pin=	20362120030000
or pin=	20364120340000
or pin=	20364140090000
or pin=	21301060020000
or pin=	21301170070000
or pin=	21303000230000
or pin=	21303160030000
or pin=	21303160070000
or pin=	21303190080000
or pin=	25022030120000
or pin=	25022110250000
or pin=	25022140100000
or pin=	20244040310000
or pin=	20252060060000
or pin=	20252130130000
or pin=	20252150020000
or pin=	20252200220000
or pin=	20252220130000
or pin=	20252230160000
or pin=	20353190340000
or pin=	21311030620000
or pin=	21311090040000
or pin=	21312270020000
or pin=	21312320130000
or pin=	21313180030000
or pin=	21313190230000
or pin=	21314110380000
or pin=	21314120210000
or pin=	26061040360000
or pin=	26062010300000
or pin=	26062090060000
or pin=	26062150070000
or pin=	26062160370000
or pin=	26081020080000
or pin=	20273040390000
or pin=	20273160270000
or pin=	20341110070000
or pin=	20343000360000
or pin=	20343030170000
or pin=	20343150250000
or pin=	20343220260000
or pin=	25031000350000
or pin=	25031010020000
or pin=	25031010330000
or pin=	25031160120000
or pin=	25033080130000
or pin=	25101020290000
or pin=	25101030310000
or pin=	25101090040000
or pin=	25101240220000
or pin=	20342050100000
or pin=	20342110260000
or pin=	20342120210000
or pin=	20342240170000
or pin=	20342250190000
or pin=	20342310160000
or pin=	20344000370000
or pin=	20344020050000
or pin=	20351080070000
or pin=	20351090210000
or pin=	20351140200000
or pin=	20351150070000
or pin=	25032110190000
or pin=	25032180370000
or pin=	25032220060000
or pin=	25032300380000
or pin=	25034000420000
or pin=	25034100360000
or pin=	25034110260000
or pin=	25034110310000
or pin=	25034260010000
or pin=	25034310290000
or pin=	25021030140000
or pin=	25021050480000
or pin=	25024150190000
or pin=	25011230330000
or pin=	25011280020000
or pin=	25011290540000
or pin=	25011320220000
or pin=	25012130370000
or pin=	25014210090000
or pin=	25014240140000
or pin=	26063090140000
or pin=	26063200100000
or pin=	26064140270000
or pin=	26064150080000
or pin=	26071170450000
or pin=	26071180210000
or pin=	21322010380000
or pin=	21322010410000
or pin=	21322070220000
or pin=	26051010290000
or pin=	26051130200000
or pin=	26062030120000
or pin=	26064050300000
or pin=	20243040080000
or pin=	20243040140000
or pin=	20243100080000
or pin=	20243120100000
or pin=	20243200140000
or pin=	25112020240000
or pin=	25112050140000
or pin=	25121020620000
or pin=	25124240370000
or pin=	25132080290000
or pin=	25103090070000
or pin=	25153110010000
or pin=	25221110010000
or pin=	25221150080000
or pin=	25223030890000
or pin=	25223110240000
or pin=	25104080100000
or pin=	25152080450000
or pin=	25154130580000
or pin=	25152180210000
or pin=	25152220200000
or pin=	25154050560000
or pin=	25222180540000
or pin=	25222260310000
or pin=	26171140190000
or pin=	26182040110000
or pin=	25341130240000
or pin=	26312170110000
or pin=	26312180210000
or pin=	26314130160000
or pin=	17212110500000
or pin=	17223150390000
or pin=	17221091290000
or pin=	17281310350000
or pin=	17281320050000
or pin=	17282090160000
or pin=	17282190290000
or pin=	17282300180000
or pin=	17283200090000
or pin=	17283210370000
or pin=	17283220190000
or pin=	17283240340000
or pin=	17283250180000
or pin=	17283270220000
or pin=	17283310130000
or pin=	17284150140000
or pin=	17284150430000
or pin=	17284160130000
or pin=	17331020410000
or pin=	17331090100000
or pin=	17331090490000
or pin=	17331220870000
or pin=	17332090160000
or pin=	17333060150000
or pin=	17333060160000
or pin=	17333060170000
or pin=	17333080290000
or pin=	17333090170000
or pin=	17333130450000
or pin=	17333260090000
or pin=	17351010840000
or pin=	17351040140000
or pin=	17341200330000
or pin=	17343090290000
or pin=	17343090680000
or pin=	17343120250000
or pin=	17343120270000
or pin=	17343180190000
or pin=	17343260060000
or pin=	17343270010000
or pin=	17343280260000
or pin=	17341050310000
or pin=	17293130150000
or pin=	17293160090000
or pin=	17293200330000
or pin=	17293250250000
or pin=	17293290080000
or pin=	17294090330000
or pin=	17294130380000
or pin=	17294220740000
or pin=	17294240310000
or pin=	17294240640000
or pin=	17294250650000
or pin=	17321030510000
or pin=	17321040320000
or pin=	17321060030000
or pin=	17321190120000
or pin=	17322020230000
or pin=	17322030320000
or pin=	17322060160000
or pin=	17322070380000
or pin=	17322160760000
or pin=	17322170700000
or pin=	17322171360000
or pin=	17322190210000
or pin=	17322220400000
or pin=	17322230220000
or pin=	17322250110000
or pin=	17322270060000
or pin=	17324130170000
or pin=	16354090140000
or pin=	16354110400000
or pin=	16363160160000
or pin=	16363180330000
or pin=	16364000090000
or pin=	16364010120000
or pin=	16364010380000
or pin=	16364030370000
or pin=	16364260130000
or pin=	16364270430000
or pin=	17311060130000
or pin=	17312230420000
or pin=	17313070250000
or pin=	17314070070000
or pin=	17314080420000
or pin=	17314110040000
or pin=	17314110160000
or pin=	17314110450000
or pin=	17314210480000
or pin=	17314270070000) bs=1.
*select if bs=0.

compute bsf=sqftb.
Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute apt=0.
If class=11 or class=12 apt=1.
Compute aptbsf=apt*bsf.
Compute naptbsf=n*aptbsf.

compute bsf02=bsf*class02.
compute bsf03=bsf*class03.
compute bsf04=bsf*class04.
compute bsf05=bsf*class05.
compute bsf06=bsf*class06.
compute bsf07=bsf*class07.
compute bsf08=bsf*class08.
compute bsf09=bsf*class09.
compute bsf10=bsf*class10.
compute bsf11=bsf*class11.
compute bsf12=bsf*class12.
compute bsf34=bsf*class34.
compute bsf78=bsf*class78.
compute bsf95=bsf*class95.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
compute nbsf1095=n*cl1095*sqrt(bsf).


compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

compute cl95=0.
if class=95 cl95=1.                  	
compute nbsf95=n*cl95*sqrt(bsf).

*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile789101112.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.


****************************************************************************************************************.
* The next section computes a  mid-frequency foreclosure zone .
* and a high frequency foreclosure zone based on foreclosure rates. 
* in specific Hyde Park and South neighborhoods (see Hyde Park and South foreclosure maps).
********************************************************************************************.

Compute lowzonehydepark=0.
if town=70 and (nghcde=20 or nghcde=91 or nghcde=101  or nghcde=150  or nghcde=151
or nghcde=240 or nghcde=241  or nghcde=280) lowzonehydepark=1.                         	

Compute midzonehydepark=0.
if town=70 and (nghcde=80 or nghcde=83  or nghcde=111  or nghcde=120 or  nghcde=130
or nghcde=170 or nghcde=180 or nghcde=220 or nghcde=230 or nghcde=250 or nghcde=260) midzonehydepark=1. 

Compute highzonehydepark=0.
if town=70 and (nghcde=10 or nghcde=30  or nghcde=70  or nghcde=100   or nghcde=121
or nghcde=140 or nghcde=210 ) highzonehydepark=1.


Compute srfxlowblockhydepark=0.
if lowzonehydepark=1 srfxlowblockhydepark=srfx*lowzonehydepark.

Compute srfxmidblockhydepark=0.
if midzonehydepark=1 srfxmidblockhydepark=srfx*midzonehydepark.

Compute srfxhighblockhydepark=0.
if highzonehydepark=1 srfxhighblockhydepark=srfx*highzonehydepark.


Compute lowzonesouth=0.
if town=76 and (nghcde=10 or nghcde=11 or nghcde=12  or nghcde=30  
or nghcde=42) lowzonesouth=1. 

Compute midzonesouth=0.
if town=76 and (nghcde=50  or nghcde=60)  midzonesouth=1.   

Compute highzonesouth=0.
if town=76 and (nghcde=40  or nghcde=41 )  highzonesouth=1.


Compute srfxlowblocksouth=0.
if lowzonesouth=1 srfxlowblocksouth=srfx*lowzonesouth.

Compute srfxmidblocksouth=0.
if midzonesouth=1 srfxmidblocksouth=srfx*midzonesouth.

Compute srfxhighblocksouth=0.
if highzonesouth=1 srfxhighblocksouth=srfx*highzonesouth.




*********************************************************************************************.

Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute jantmar07=0.
if (year1=2007 and (mos>=1 and mos<=3)) jantmar07=1. 
Compute octtdec11=0.
if (year1=2011 and (mos>=10 and mos<=12)) octtdec11=1.


Compute jantmar07cl234=jantmar07*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute octtdec11cl234=octtdec11*cl234.

Compute jantmar07cl56=jantmar07*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute octtdec11cl56=octtdec11*cl56.

Compute jantmar07cl778=jantmar07*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute octtdec11cl778=octtdec11*cl778.

Compute jantmar07cl89=jantmar07*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute octtdec11cl89=octtdec11*cl89.


Compute jantmar07cl1112=jantmar07*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute octtdec11cl1112=octtdec11*cl1112.


Compute jantmar07cl1095=jantmar07*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute octtdec11cl1095=octtdec11*cl1095.

Compute jantmar07clsplt=jantmar07*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute octtdec11clsplt=octtdec11*clsplt.


compute tnb=(town*1000) + nghcde.

If (tnb=70010) n=2.38. 
If (tnb=70020) n=3.90.
If (tnb=70030) n=1.63.
If (tnb=70070) n=1.90. 
If (tnb=70080) n=1.62. 
If (tnb=70083) n=2.45. 
If (tnb=70091) n=1.28. 
If (tnb=70100) n=1.36. 
If (tnb=70101) n=1.19.
If (tnb=70111) n=1.57. 
If (tnb=70120) n=1.55. 
If (tnb=70121) n=1.38. 
If (tnb=70130) n=1.52. 
If (tnb=70140) n=1.63. 
If (tnb=70150) n=4.12.
If (tnb=70151) n=1.72. 
If (tnb=70170) n=1.12. 
If (tnb=70180) n=1.10. 
If (tnb=70210) n=1.35. 
If (tnb=70220) n=1.26. 
If (tnb=70230) n=1.43. 
If (tnb=70240) n=1.09. 
If (tnb=70241) n=2.53. 
If (tnb=70250) n=1.18. 
If (tnb=70260) n=1.00.
If (tnb=70280) n=1.34.
if (tnb=76010) n=4.75. 
if (tnb=76011) n=4.30.
if (tnb=76012) n=4.90.
if (tnb=76030) n=2.86.
if (tnb=76040) n=1.70.
if (tnb=76041) n=2.33.
if (tnb=76042) n=3.04.
if (tnb=76050) n=2.46.
if (tnb=76060) n=1.78.


compute sb70010=0.
compute sb70020=0.
compute sb70030=0.
compute sb70070=0.
compute sb70080=0.
compute sb70083=0.
compute sb70091=0.
compute sb70100=0.
compute sb70101=0.
compute sb70111=0.
compute sb70120=0.
compute sb70121=0.
compute sb70130=0.
compute sb70140=0.
compute sb70150=0.
compute sb70151=0.
compute sb70170=0.
compute sb70180=0.
compute sb70210=0.
compute sb70220=0.
compute sb70230=0.
compute sb70240=0.
compute sb70241=0.
compute sb70250=0.
compute sb70260=0.
compute sb70280=0.
compute sb70300=0.
compute sb76010=0.
compute sb76011=0.
compute sb76012=0.
compute sb76030=0.
compute sb76040=0.
compute sb76041=0.
compute sb76042=0.
compute sb76050=0.
compute sb76060=0.


if (tnb=70010) sb70010=sqrt(bsf).
if (tnb=70020) sb70020=sqrt(bsf).
if (tnb=70030) sb70030=sqrt(bsf).
if (tnb=70070) sb70070=sqrt(bsf).
if (tnb=70080) sb70080=sqrt(bsf).
if (tnb=70083) sb70083=sqrt(bsf).
if (tnb=70091) sb70091=sqrt(bsf).
if (tnb=70100) sb70100=sqrt(bsf).
if (tnb=70101) sb70101=sqrt(bsf).
if (tnb=70111) sb70111=sqrt(bsf).
if (tnb=70120) sb70120=sqrt(bsf).
if (tnb=70121) sb70121=sqrt(bsf).
if (tnb=70130) sb70130=sqrt(bsf).
if (tnb=70140) sb70140=sqrt(bsf).
if (tnb=70150) sb70150=sqrt(bsf).
if (tnb=70151) sb70151=sqrt(bsf).
if (tnb=70170) sb70170=sqrt(bsf).
if (tnb=70180) sb70180=sqrt(bsf).
if (tnb=70210) sb70210=sqrt(bsf).
if (tnb=70220) sb70220=sqrt(bsf).
if (tnb=70230) sb70230=sqrt(bsf).
if (tnb=70240) sb70240=sqrt(bsf).
if (tnb=70241) sb70241=sqrt(bsf).
if (tnb=70250) sb70250=sqrt(bsf).
if (tnb=70260) sb70260=sqrt(bsf).
if (tnb=70280) sb70280=sqrt(bsf).
if (tnb=76010) sb76010=sqrt(bsf).
if (tnb=76011) sb76011=sqrt(bsf).
if (tnb=76012) sb76012=sqrt(bsf).
if (tnb=76030) sb76030=sqrt(bsf).
if (tnb=76040) sb76040=sqrt(bsf).
if (tnb=76041) sb76041=sqrt(bsf).
if (tnb=76042) sb76042=sqrt(bsf).
if (tnb=76050) sb76050=sqrt(bsf).
if (tnb=76060) sb76060=sqrt(bsf).

compute n70010=0.
compute n70020=0.
compute n70030=0.
compute n70070=0.
compute n70080=0.
compute n70083=0.
compute n70091=0.
compute n70100=0.
compute n70101=0.
compute n70111=0.
compute n70120=0.
compute n70121=0.
compute n70130=0.
compute n70140=0.
compute n70150=0.
compute n70151=0.
compute n70170=0.
compute n70180=0.
compute n70210=0.
compute n70220=0.
compute n70230=0.
compute n70240=0.
compute n70241=0.
compute n70250=0.
compute n70260=0.
compute n70280=0.
compute n76010=0.
compute n76011=0.
compute n76012=0.
compute n76030=0.
compute n76040=0.
compute n76041=0.
compute n76042=0.
compute n76050=0.
compute n76060=0.


if (tnb=70010) n70010=1.0.
if (tnb=70020) n70020=1.0.
if (tnb=70030) n70030=1.0.
if (tnb=70070) n70070=1.0.
if (tnb=70080) n70080=1.0.
if (tnb=70083) n70083=1.0.
if (tnb=70091) n70091=1.0.
if (tnb=70100) n70100=1.0.
if (tnb=70101) n70101=1.0.
if (tnb=70111) n70111=1.0.
if (tnb=70120) n70120=1.0.
if (tnb=70121) n70121=1.0.
if (tnb=70130) n70130=1.0.
if (tnb=70140) n70140=1.0.
if (tnb=70150) n70150=1.0.
if (tnb=70151) n70151=1.0.
if (tnb=70170) n70170=1.0.
if (tnb=70180) n70180=1.0.
if (tnb=70210) n70210=1.0.
if (tnb=70220) n70220=1.0.
if (tnb=70230) n70230=1.0.
if (tnb=70240) n70240=1.0.
if (tnb=70241) n70241=1.0.
if (tnb=70250) n70250=1.0.
if (tnb=70260) n70260=1.0.
if (tnb=70280) n70280=1.0.
if (tnb=76010) n76010=1.0.
if (tnb=76011) n76011=1.0.
if (tnb=76012) n76012=1.0.
if (tnb=76030) n76030=1.0.
if (tnb=76040) n76040=1.0.
if (tnb=76041) n76041=1.0.
if (tnb=76042) n76042=1.0.
if (tnb=76050) n76050=1.0.
if (tnb=76060) n76060=1.0.

               	
Compute lsf=sqftl.
Compute nlsf=n*lsf.
Compute nbsf=n*bsf.
Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*srbsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=npremrf*bsf.

If firepl>0 firepl=1.
compute nfirepl=n*firepl.

compute nogarage=0.
if gar=7 nogarage=1.
Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.

Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=0.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

if tnb=70010 and class=95 lsf=1195.
if tnb=70020 and class=95 lsf=1764.
if tnb=70070 and class=95 lsf=1526.
if tnb=70080 and class=95 lsf=1058.
if tnb=70100 and class=95 lsf=2085.
if tnb=70111 and class=95 lsf=2740.
if tnb=70120 and class=95 lsf=1657.
if tnb=70121 and class=95 lsf=1792.
if tnb=70180 and class=95 lsf=3095.
if tnb=70260 and class=95 lsf=2502.
if tnb=76011 and class=95 lsf=1121.
if tnb=76012 and class=95 lsf=1007.
if tnb=76030 and class=95 lsf=1176.
if tnb=76040 and class=95 lsf=875.
if tnb=76041 and class=95 lsf=1987.
if tnb=76042 and class=95 lsf=1710.
if tnb=76051 and class=95 lsf=1152.
if tnb=76060 and class=95 lsf=1059.

compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.
compute nfulbath=n*fullbath.
compute nhafbath=n*halfbath.
compute nbathsum=n*bathsum.

If num=6 bnum=0.
If num=0 bnum=0.
If num=1 bnum=2.
If num=2 bnum=3.
If num=3 bnum=4.
If num=4 bnum=5.
If num=5 bnum=6.
compute nbnum=n*bnum.
compute bnumb=bnum*bsf.

sel if town=76.

compute reg = ( -26235.860706	
+ 6325.261136*nbathsum
+ 2.854995*nbsfair
+ 202.621545*nsrlsf
- 2771.357879*sb70150
+ 8412.362761*nbsf778
- 55888.731580*midzonehydepark
- 4373.122381*lowzonehydepark
- 63544.654188*highzonehydepark
+ 3635.965324*nbsf234
+ 90916.045940*nbasfull
+ 3532.523317*nfirepl
- 3.434119*nbsf
- 4440.334833*srfxmidblocksouth
+ 3864.019364*garage1
+ 6.758453*masbsf
+ 467.036760*sb70010
+ 374.383078*sb70030
+ 4.562645*frmsbsf
- 3196.285448*nsrage
+ 3.224833*nprembsf
+ 2277.209841*nbsf1112
- 3897.760517*srfxhighblocksouth
- 1939.816000*sb76041
+ 12385.344532*nbsf89
+ 82177.354714*nbaspart
+ 72576.507299*nnobase
- 2471.768984*sb70241
+ 3980.554300*sb70020
+ 5.992003*stubsf
+ 5886.898255*nbsf56
- 862.311643*sb70280
+ 7892.300044*garage2
- 12459.194056*srfxlowblockhydepark
+ 401.665139*sb76060
- 15523.016234*highzonesouth
- 316.183918*sb70083
+ 3483.187274*nbsf1095
+ 5952.325505*nbsf34
- 2865.272231*srfxhighblockhydepark
- 829.916031*sb70101
- 423.151515*sb70210
- 6240.717702*srfxlowblocksouth
+ 14600.878733*biggar
+ 652.722675*sb70170
+ 239.665787*sb70130
- 4834.688126*srfxmidblockhydepark)*1.0.



*SAVE OUTFILE='C:\Program Files\IBM\SPSS\Statistics\19\1\mv76.sav'/keep town pin mv.	