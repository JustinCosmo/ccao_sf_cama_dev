                       *SPSS Regression.
	                         *MAINE 2016.

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt22apr4.sav'.
*select if (amount1>190000).
*select if (amount1<950000).
*select if (multi<1).
*select if sqftb<7000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=15 and amount1>0)  year1=2015.
If (yr=14 and amount1>0)  year1=2014.
If (yr=13 and amount1>0)  year1=2013.  
If (yr=12 and amount1>0)  year1=2012.
If (yr=11 and amount1>0)  year1=2011.
If (yr=10 and amount1>0)  year1=2010. 
If  (yr=9 and amount1>0)  year1=2009. 
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  

*select if (year1>2010).
set mxcells=2000500.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	9071010560000
or pin=	9181150160000
or pin=	9173040270000
or pin=	9173120290000
or pin=	9141100170000
or pin=	9151020470000
or pin=	9164010790000
or pin=	9131000670000
or pin=	9132100100000
or pin=	9132150110000
or pin=	9243060090000
or pin=	9244070330000
or pin=	9252220270000
or pin=	9112020350000
or pin=	9114270010000
or pin=	9114310120000
or pin=	9121050130000
or pin=	9123000260000
or pin=	9123040450000
or pin=	9124070150000
or pin=	9124310030000
or pin=	9124350040000
or pin=	9124440290000
or pin=	9231140060000
or pin=	9231010190000
or pin=	9231030930000
or pin=	9231031040000
or pin=	9231170640000
or pin=	9234000430000
or pin=	9203160160000
or pin=	9293021700000
or pin=	9221150040000
or pin=	9222050110000
or pin=	9222070440000
or pin=	9224060040000
or pin=	9224140110000
or pin=	9262010180000
or pin=	9262090050000
or pin=	9262120180000
or pin=	9214040150000
or pin=	9221090200000
or pin=	9221200110000
or pin=	9221200380000
or pin=	9221210320000
or pin=	9223000450000
or pin=	9223110300000
or pin=	9224080630000
or pin=	9263210270000
or pin=	9271030010000
or pin=	9271120110000
or pin=	9271220210000
or pin=	9271250340000
or pin=	9271290030000
or pin=	9272170200000
or pin=	9274010160000
or pin=	9274140150000
or pin=	9251090400000
or pin=	9253240040000
or pin=	9253240150000
or pin=	9253250160000
or pin=	9253260200000
or pin=	9262050090000
or pin=	9262050640000
or pin=	9262050660000
or pin=	9264000260000
or pin=	9264000360000
or pin=	9264040010000
or pin=	9264050440000
or pin=	9264060370000
or pin=	9264150050000
or pin=	9304010430000
or pin=	9304070170000
or pin=	9322000700000
or pin=	9344001050000
or pin=	9094010950000
or pin=	9103010780000
or pin=	9273061400000
or pin=	9274170910000
or pin=	9212000490000
or pin=	9344020120000
or pin=	9351110120000
or pin=	9351140150000
or pin=	9351240180000
or pin=	9352230210000
or pin=	9353050050000
or pin=	9353110630000
or pin=	9353110650000
or pin=	9353180200000
or pin=	9353230280000
or pin=	9353260260000
or pin=	9354000240000
or pin=	9354010200000
or pin=	9354010230000
or pin=	9354140220000
or pin=	9354160050000
or pin=	9354160090000
or pin=	9361000170000
or pin=	9361010220000
or pin=	9363060200000
or pin=	9363170230000
or pin=	9363300280000
or pin=	9161060180000
or pin=	9211030250000
or pin=	9213000240000)  bs=1.
*select if bs=0.

compute bsf=sqftb.
Compute N=1.


*COMPUTE FX = cumfile13141516.
*IF FX > 9  FX = 9.
*COMPUTE SRFX = sqrt(FX).
*RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute tnb=(town*1000) + nghcde.

if (tnb= 22010 ) n=3.11.
if (tnb= 22022 ) n=2.52.
if (tnb= 22023 ) n=2.65.
if (tnb= 22030 ) n=2.34.
if (tnb= 22031 ) n=2.53.
if (tnb= 22040 ) n=2.43.
if (tnb= 22050 ) n=2.52.
if (tnb= 22060 ) n=3.45.
if (tnb= 22070 ) n=3.22.
if (tnb= 22080 ) n=2.66.
if (tnb= 22082 ) n=2.87.
if (tnb= 22085 ) n=3.29.
if (tnb= 22087 ) n=3.46.
if (tnb= 22091 ) n=2.51. 
if (tnb= 22093 ) n=2.67.
if (tnb= 22100 ) n=4.01.
if (tnb= 22110 ) n=4.25.
if (tnb= 22111 ) n=3.50.
if (tnb= 22120 ) n=5.50.
if (tnb= 22130 ) n=3.50.
if (tnb= 22140 ) n=3.65.
if (tnb= 22141 ) n=4.95.
if (tnb= 22142 ) n=7.61.
if (tnb= 22143 ) n=4.32.
if (tnb= 22145 ) n=4.35.
if (tnb= 22150 ) n=4.75.
if (tnb= 22180 ) n=3.27.


compute sb22010=0.
compute sb22022=0.
compute sb22023=0.
compute sb22030=0.
compute sb22031=0.
compute sb22040=0.
compute sb22050=0.
compute sb22060=0.
compute sb22070=0.
compute sb22080=0.
compute sb22082=0.
compute sb22085=0.
compute sb22087=0.
compute sb22091=0.
compute sb22093=0. 
compute sb22100=0.
compute sb22110=0.
compute sb22111=0.
compute sb22112=0.
compute sb22120=0.
compute sb22130=0.
compute sb22140=0.
compute sb22141=0.
compute sb22142=0.
compute sb22143=0.
compute sb22145=0.
compute sb22150=0.
compute sb22180=0.


compute n22010=0.
compute n22022=0.
compute n22023=0.
compute n22030=0.
compute n22031=0.
compute n22040=0.
compute n22050=0.
compute n22060=0.
compute n22070=0.
compute n22080=0.
compute n22082=0.
compute n22085=0.
compute n22087=0.
compute n22091=0.
compute n22093=0.
compute n22100=0. 
compute n22110=0.
compute n22111=0.
compute n22112=0.
compute n22120=0.
compute n22130=0.
compute n22140=0.
compute n22141=0.
compute n22142=0.
compute n22143=0.
compute n22145=0.
compute n22150=0.
compute n22180=0.



if (tnb= 22010 ) n22010=1.
if (tnb= 22022 ) n22022=1.
if (tnb= 22023 ) n22023=1.
if (tnb= 22030 ) n22030=1.
if (tnb= 22031 ) n22031=1.
if (tnb= 22040 ) n22040=1.
if (tnb= 22050 ) n22050=1.
if (tnb= 22060 ) n22060=1.
if (tnb= 22070 ) n22070=1.
if (tnb= 22080 ) n22080=1.
if (tnb= 22082 ) n22082=1.
if (tnb= 22085 ) n22085=1.
if (tnb= 22087 ) n22087=1.
if (tnb= 22091 ) n22091=1.
if (tnb= 22093 ) n22093=1.
if (tnb= 22100 ) n22100=1.
if (tnb= 22110 ) n22110=1.
if (tnb= 22111 ) n22111=1.
if (tnb= 22112 ) n22112=1.
if (tnb= 22120 ) n22120=1.
if (tnb= 22130 ) n22130=1.
if (tnb= 22140 ) n22140=1.
if (tnb= 22141 ) n22141=1.
if (tnb= 22142 ) n22142=1.
if (tnb= 22143 ) n22143=1.
if (tnb= 22145 ) n23145=1.
if (tnb= 22150 ) n22150=1.
if (tnb= 22180 ) n22180=1.



if (tnb= 22010 ) sb22010=sqrt(bsf).
if (tnb= 22022 ) sb22022=sqrt(bsf).
if (tnb= 22023 ) sb22023=sqrt(bsf).
if (tnb= 22030 ) sb22030=sqrt(bsf).
if (tnb= 22031 ) sb22031=sqrt(bsf).
if (tnb= 22040 ) sb22040=sqrt(bsf).
if (tnb= 22050 ) sb22050=sqrt(bsf).
if (tnb= 22060 ) sb22060=sqrt(bsf).
if (tnb= 22070 ) sb22070=sqrt(bsf).
if (tnb= 22080 ) sb22080=sqrt(bsf).
if (tnb= 22082 ) sb22082=sqrt(bsf).
if (tnb= 22085 ) sb22085=sqrt(bsf).
if (tnb= 22087 ) sb22087=sqrt(bsf).
if (tnb= 22091 ) sb22091=sqrt(bsf).
if (tnb= 22093 ) sb22093=sqrt(bsf).
if (tnb= 22100 ) sb22100=sqrt(bsf).
if (tnb= 22110 ) sb22110=sqrt(bsf).
if (tnb= 22111 ) sb22111=sqrt(bsf).
if (tnb= 22112 ) sb22112=sqrt(bsf).
if (tnb= 22120 ) sb22120=sqrt(bsf).
if (tnb= 22130 ) sb22130=sqrt(bsf).
if (tnb= 22140 ) sb22140=sqrt(bsf).
if (tnb= 22141 ) sb22141=sqrt(bsf).
if (tnb= 22142 ) sb22142=sqrt(bsf).
if (tnb= 22143 ) sb22143=sqrt(bsf).
if (tnb= 22145 ) sb23145=sqrt(bsf).
if (tnb= 22150 ) sb22150=sqrt(bsf).
if (tnb= 22180 ) sb22180=sqrt(bsf).

*Compute lowzonemaine=0.
*if town=22 and  (nghcde=10 or nghcde=22 or nghcde=87 or nghcde=110 or nghcde=111 
or nghcde=120 or nghcde=141 or nghcde=142 or nghcde=143 or nghcde=150)  lowzonemaine=1.                         	

*Compute midzonemaine=0.
*if town=22 and (nghcde=23 or nghcde=30 or nghcde=31 
or nghcde=70 or nghcde=80 or nghcde=85 or nghcde=93
or nghcde=111 or nghcde=130 or nghcde=140 or nghcde=180)   midzonemaine=1. 

*Compute highzonemaine=0.
*if town=22 and (nghcde=40 or nghcde=50 or nghcde=60  
or nghcde=82 or nghcde=91 or nghcde=100 or nghcde=145)   highzonemaine=1.


*Compute srfxlowblockmaine=0.
*if lowzonemaine=1 srfxlowblockmaine=srfx*lowzonemaine.

*Compute srfxmidblockmaine=0.
*if midzonemaine=1 srfxmidblockmaine=srfx*midzonemaine.

*Compute srfxhighblockmaine=0.
*if highzonemaine=1 srfxhighblockmaine=srfx*highzonemaine.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1112=0.
if (mos > 9 and yr=11 or (mos <= 3 and yr=12)) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1. 
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute jantmar11=0.
if (year1=2011 and (mos>=1 and mos<=3)) jantmar11=1. 
Compute octtdec15=0.
if (year1=2015 and (mos>=10 and mos<=12)) octtdec15=1.

Compute jantmar11cl234=jantmar11*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute octtdec15cl234=octtdec15*cl234.

Compute jantmar11cl56=jantmar11*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute octtdec15cl56=octtdec15*cl56.

Compute jantmar11cl778=jantmar11*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute octtdec15cl778=octtdec15*cl778.

Compute jantmar11cl89=jantmar11*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute octtdec15cl89=octtdec15*cl89.


Compute jantmar11cl1112=jantmar11*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute octtdec15cl1112=octtdec15*cl1112.

Compute jantmar11cl1095=jantmar11*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute octtdec15cl1095=octtdec15*cl1095.

Compute jantmar11clsplt=jantmar11*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute octtdec15clsplt=octtdec15*clsplt.


Compute lsf=sqftl.

if class=95 and tnb=22031   lsf=2934.
if class=95 and tnb=22040   lsf=2093.
if class=95 and tnb=22050   lsf=2952.
if class=95 and tnb=22085   lsf=2048.
if class=95 and tnb=22091   lsf=1150.
if class=95 and tnb=22110   lsf=1200.
if class=95 and tnb=22111   lsf=2155.	
if class=95 and tnb=22130   lsf=1788.	
if class=95 and tnb=22180   lsf=2600.

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).

If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (town=22 and year1>=2015).
*select if puremarket=1.
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').


compute mv = (184093.6956	
+ 4549.416614*nbathsum
+ 0.524692347*nbsfair
+ 1848.163268*nbsf778
+ 208.8369195*nsrlsf
+ 10.51598832*masbsf
+ 1959.793786*nbsf56
+ 1983.663503*nbsf89
+ 1942.537674*sb22111
+ 2045.133601*sb22150
- 4244.422785*nsrage
+ 1297.82282*sb22085
+ 3628.255348*nfirepl
+ 971.4699312*sb22080
+ 3836.195498*sb22143
+ 1247.108019*sb22120
+ 1143.200418*sb22110
+ 1990.866795*sb22100
- 18994.23281*garnogar
- 39730.25059*nnobase
+ 664.306866*sb22141
- 14879.05558*garage1
- 2102.326938*sb22140
+ 6.662640382*frabsf
+ 543.8084133*sb22087
+ 479.9938314*sb22070
+ 1608.658926*nbsf234
+ 1769.489726*nbsf34
+ 1596.519046*nbsf1112
+ 1295.597156*nbsf1095
- 32968.99613*nbaspart
- 28078.27614*nbasfull
- 1616.369515*sb22060
- 548.4943156*sb22130
+ 18859.00505*biggar
- 6078.735357*garage2
+ 1.111882906*nprembsf
- 0.227938506*frastbsf
+ 39.95841043*totunitb
+ 439.2654432*sb22022
+ 480.8179673*sb22030
+ 304.5148533*sb22031)*1.0.


save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv22apr5.sav'/keep town pin mv. 

