# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/. 

# Top comments ----

# This script recovers desk-reviewed outliers from the mainframe
# Version 0.2
# Written 12/22/2018
# Updated 4/15/2019

#Test queries

# dbGetQuery(CCAODATA, 
# paste0(" 
# SELECT COUNT(D65_PIN), D65_EMPID
# FROM [65D] AS D
# INNER JOIN
# HEAD AS H
# ON H.HD_PIN=D.D65_PIN 
# WHERE D.TAX_YEAR=2019 AND H.TAX_YEAR=2018 AND D65_NEW_MV!=D65_EST_MV AND D65_NEW_MV>0
# AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN ('", target_township, "')
# GROUP BY D65_EMPID
# "))
# 
#   dbGetQuery(CCAODATA, 
#              paste0(" 
# SELECT HD_PIN, D65_COMMENT, D65_DATECHG, D65_EMPID, D65_NEW_MV, D65_EST_MV
# FROM [65D] AS D
# INNER JOIN
# HEAD AS H
# ON H.HD_PIN=D.D65_PIN 
# WHERE D.TAX_YEAR=2019 AND H.TAX_YEAR=2018 AND D65_NEW_MV!=D65_EST_MV AND D65_NEW_MV>0
# AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN ('", target_township, "')
# "))


#Get DR data
get_desktop_review_results <- function(dr_township){
  dbGetQuery(CCAODATA,
             paste0("
                    SELECT HD_PIN, D65_COMMENT, D65_DATECHG, D65_EMPID, D65_NEW_MV, D65_EST_MV
                    FROM [65D] AS D
                    INNER JOIN
                    HEAD AS H
                    ON H.HD_PIN=D.D65_PIN
                    WHERE D.TAX_YEAR=2019 AND H.TAX_YEAR=2018 AND D65_NEW_MV!=D65_EST_MV AND D65_NEW_MV>0
                    AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN ('", dr_township, "')
                    "))
}


#example add_desktop_review_results(22, "O:/CCAODATA/results/final_values/scope_22_2019.Rda")
add_desktop_review_results <- function(current_township, curfile){

  desktop_review_results <- get_desktop_review_results(current_township)
  names(desktop_review_results) <- c("PIN", "DR_COMMENT", "DR_DATE",
                                     "DR_EMPID", "fitted_value_7", "fitted_value_6.2")
  desktop_review_results$fitted_value_7[desktop_review_results$fitted_value_7 == 0] <-
    desktop_review_results$fitted_value_6.2[desktop_review_results$fitted_value_7 == 0]
  load(curfile, verbose = TRUE)
  
  #reset MF pins with MULTI_IND = 1 to end in 0 for join
  valuationdata$PIN[valuationdata$MULTI_IND == 1] <- 
    paste0(substr(valuationdata$PIN[valuationdata$MULTI_IND == 1], 1, 13), 0)
  
  
  #Overwrite any previous DR data in scope
  strip_cols <- c("DR_COMMENT", "DR_DATE", "DR_EMPID",
                  "fitted_value_7", "fitted_value_6.2", "ratio_7")
  valuationdata <- valuationdata[, !names(valuationdata) %in% strip_cols]
  valuationdata <- merge(x = valuationdata, y = desktop_review_results, by = "PIN", all.x = TRUE)

  #record QA pins that were caught, make sure their values are reverted in the scope
  # if(exists('qa_pins')){
  #   if(valuationdata$fitted_value_7[valuationdata$dr_qa == 1] != qa_pins$fitted_value_qa){
  #     valuationdata$DR_COMMENT[valuationdata$dr_qa == 1] <- "DR_QA1"
  #   }
  #   valuationdata$fitted_value_7[valuationdata$dr_qa == 1] <- valuationdata$fitted_value_6[valuationdata$dr_qa == 1]
  # }
  
  #reset MF pins with MULTI_IND = 1 to not end in 0
  valuationdata$PIN[valuationdata$MULTI_IND == 1] <- 
    paste0(substr(valuationdata$PIN[valuationdata$MULTI_IND == 1], 1, 13), valuationdata$MULTI_CODE[valuationdata$MULTI_IND == 1])
  
  
  integ_check_f(length(unique(valuationdata$PIN)) != nrow(valuationdata),
                paste0("FINAL data is unique by PIN AFTER collapsing MULTI properties"),
                paste0("Bad: ", length(unique(valuationdata$PIN)) - nrow(valuationdata),
                       " Duplicate PINs in the FINAL data"),
                "GOOD: No duplicate in the valuation data",
                "7")
  
  
  #Check for pins in DR data and not in valuations data/debugging code
  extra_dr_pins <- c()
  for (pin in desktop_review_results$PIN){
    if (!(pin %in% valuationdata$PIN)) {
      if (!(substr(pin, 1, 13) %in% (substr(valuationdata$PIN, 1, 13)))){
        extra_dr_pins <- append(extra_dr_pins, pin)
      }
    }
  }
  if (!is.null(extra_dr_pins)){
    print(paste('these pins were not joined', extra_dr_pins))
  }
  
  
  valuationdata$fitted_value_6.2[is.na(valuationdata$fitted_value_6.2)] <-
    valuationdata$fitted_value_6[is.na(valuationdata$fitted_value_6.2)]
  valuationdata$fitted_value_6.2[valuationdata$fitted_value_6.2 == 0] <-
    valuationdata$fitted_value_6[valuationdata$fitted_value_6.2 == 0]
  valuationdata$fitted_value_7[is.na(valuationdata$fitted_value_7)] <-
    valuationdata$fitted_value_6[is.na(valuationdata$fitted_value_7)]
  valuationdata$fitted_value_7[valuationdata$fitted_value_7 == 0] <-
    valuationdata$fitted_value_6[valuationdata$fitted_value_7 == 0]
  valuationdata$fitted_value_7 <- as.numeric(valuationdata$fitted_value_7)
  valuationdata$ratio_7 <- valuationdata$fitted_value_7 /
    valuationdata$most_recent_sale_price
  
  #Masking Employee IDs
  fakeName <- read.csv("O:/CCAODATA/data/raw_data/fakeName.csv")
  valuationdata$DR_EMPID <- sapply(valuationdata$DR_EMPID, function(x) fakeName$new[match(x, fakeName$emp_id)])

  #Save Scope
  file_info <- update_file_info("7_recover_desktop_reviews")
  print(paste0("adding DR to ", curfile))

  save(list = scope_list[sapply(scope_list, exists, envir=environment())], file = destfile)
}

#Check all scope files for DR
file_names <- list.files("O:/CCAODATA/results/final_values/")
for (file in file_names){
  if (substring(file, 1, 6) == "scope_"){
  current <- substring(file, 7, 8)
  if (!is.na(as.integer(current))){
    curfile <- paste("O:/CCAODATA/results/final_values/scope_",
                     current, "_2019.Rda", sep = "")
    e <- new.env()
    load(curfile, e)
    if ( (if (!is.null(e$"file_info")){
      !is.null(e$file_info[e$file_info["action"] == "Desktop Review", ])
      }else{
        TRUE
        }
      ) &
        (if (!is.null(e$valuationdata)){
          ( (nrow(e$valuationdata[!is.na(e$valuationdata$DR_COMMENT), ])) == 0)
          }else{
              TRUE
            }
         )
    ){
      print(paste("No DR data in scope for", current))
      tmp_results <- get_desktop_review_results(current)
      print(paste(nrow(tmp_results), "rows of DR avaiable"))
      if (nrow(tmp_results) > 0){
        print(paste("Do you want to add DR to", current, "?"))
        choices <- c("Yes, I'm sure!", "No, wait, better not.")
        choice <- select.list(choices, title = "Are you sure?",
                              preselect = choices, multiple = FALSE)
        if (choice == "Yes, I'm sure!"){
          print(paste('adding fitted_value_7 to', current, 'at', curfile))
          add_desktop_review_results(current, curfile)
          }else{
            print("Not added")
          }
      }
    }else{
      print(paste("DR already present in scope for", current,
                  "if you want to overwrite DR run add_desktop_review_results on", current))
      }
  }
}
}