					*SPSS Regression.
					*NORTHFIELD 2010.


Get file='C:\Program Files\SPSS\spssa\regt25repsales.sav'.
set mxcells=300000.
set wid=125.
set len=59.
select if (amount1>140000).
select if (amount1<1700000).
select if (multi<1).
select if sqftb<6000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 	 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005. 
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.

select if (year1>2004).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	4074020270000
or pin=	4074030120000
or pin=	4324011060000
or pin=	4324011750000
or pin=	4324011790000
or pin=	4324090030000
or pin=	4332080310000
or pin=	4333000430000
or pin=	4333001060000
or pin=	4333001150000
or pin=	4333010470000
or pin=	4333020150000
or pin=	4333020240000
or pin=	4333020800000
or pin=	4333070200000
or pin=	4333080150000
or pin=	4333090150000
or pin=	4333110040000
or pin=	4334040260000
or pin=	4334080250000
or pin=	4053140120000
or pin=	4061030080000
or pin=	4063010370000
or pin=	4063090120000
or pin=	4064090150000
or pin=	4072110070000
or pin=	4164050210000
or pin=	4102020150000
or pin=	4104060130000
or pin=	4104060170000
or pin=	4173001620000
or pin=	4173050030000
or pin=	4184040260000
or pin=	4202010110000
or pin=	4093130030000
or pin=	4161000080000
or pin=	4161000210000
or pin=	4161040050000
or pin=	4161080210000
or pin=	4161100110000
or pin=	4163010100000
or pin=	4163060010000
or pin=	4163060100000
or pin=	4171010730000
or pin=	4172020430000
or pin=	4172080080000
or pin=	4172110100000
or pin=	4174080020000
or pin=	4031040090000
or pin=	4031060030000
or pin=	4302020070000
or pin=	4302020090000
or pin=	4304080310000
or pin=	4092010640000
or pin=	4093050110000
or pin=	4093070190000
or pin=	4093070210000
or pin=	4093110130000
or pin=	4093120140000
or pin=	4101010420000
or pin=	4101020070000
or pin=	4101020090000
or pin=	4101030250000
or pin=	4101080100000
or pin=	4101190020000
or pin=	4112110390000
or pin=	4112160220000
or pin=	4112180510000
or pin=	4112190240000
or pin=	4112200260000
or pin=	4112220370000
or pin=	4112230240000
or pin=	4162070160000
or pin=	4162080160000
or pin=	4162090150000
or pin=	4162110040000
or pin=	4162120140000
or pin=	4162120240000
or pin=	4162160120000
or pin=	4162160240000
or pin=	4114030370000
or pin=	4242180090000
or pin=	4244140100000
or pin=	4262040390000
or pin=	4264010540000
or pin=	4264050190000
or pin=	4211120260000
or pin=	4213011320000
or pin=	4214040160000
or pin=	4214090100000
or pin=	4292030100000
or pin=	4294070010000
or pin=	4322000470000
or pin=	4324040110000
or pin=	4251030130000
or pin=	4251080320000
or pin=	4251090300000
or pin=	4251160450000
or pin=	4253070290000
or pin=	4253100010000
or pin=	4253100290000
or pin=	4253100350000
or pin=	4253110150000
or pin=	4253160190000
or pin=	4253180300000
or pin=	4341010170000
or pin=	4341020150000
or pin=	4341020300000
or pin=	4341030090000
or pin=	4342150030000
or pin=	4342150060000
or pin=	4351200020000
or pin=	4351230130000
or pin=	4092070210000
or pin=	4092110010000
or pin=	4092110030000
or pin=	4094060400000
or pin=	4094100160000
or pin=	4094160670000
or pin=	4343020200000
or pin=	4343050080000
or pin=	4343050090000
or pin=	4344000170000
or pin=	4344010080000
or pin=	4344010150000
or pin=	4344010260000
or pin=	4344010450000
or pin=	4344090140000
or pin=	4344130500000
or pin=	4344140040000
or pin=	4344150230000
or pin=	4344150300000
or pin=	4353060030000
or pin=	4353060210000
or pin=	4353080060000
or pin=	4353180160000
or pin=	4353240020000
or pin=	4353250070000
or pin=	4352020250000
or pin=	4352070660000
or pin=	4352110100000
or pin=	4151010170000
or pin=	4211010120000
or pin=	4211080190000
or pin=	4212090220000
or pin=	4223000190000
or pin=	4233030380000
or pin=	4271100240000
or pin=	4274030070000
or pin=	4274060040000
or pin=	4274220020000
or pin=	4284000600000
or pin=	4284000780000
or pin=	4284000820000
or pin=	4354070070000
or pin=	4354080680000
or pin=	4354081340000
or pin=	4363040070000
or pin=	4363050250000
or pin=	4363050300000
or pin=	4363120260000
or pin=	4363160010000
or pin=	4131120340000
or pin=	4234010910000
or pin=	4241010760000
or pin=	4241030350000
or pin=	4251050010000
or pin=	4253000290000
or pin=	4253050290000
or pin=	4261030100000
or pin=	4262030710000
or pin=	4043040950000
or pin=	4283070050000
or pin=	4283080050000
or pin=	4283090010000
or pin=	4334100010000
or pin=	4334110050000
or pin=	4043020130000
or pin=	4043030130000
or pin=	4171030210000
or pin=	4171070180000
or pin=	4171090150000
or pin=	4183020020000
or pin=	4113040010000
or pin=	4081000300000
or pin=	4081030020000
or pin=	4081050080000) bs=1.                           
select if bs=0.
compute bsf=sqftb.
Compute N=1.


*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile78910.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if puremarket=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute n=1.

Compute n10=0.
If nghcde=10 n10=1.
Compute n11=0.
If nghcde=11 n11=1.
Compute n21=0.
If nghcde=21 n21=1.
Compute n23=0.
If nghcde=23 n23=1.
Compute n30=0.
If nghcde=30 n30=1.
Compute n51=0.
If nghcde=51 n51=1.
Compute n52=0.
If nghcde=52 n52=1.
Compute n61=0.
If nghcde=61 n61=1.
Compute n72=0.
If nghcde=72 n72=1.
Compute n82=0.
If nghcde=82 n82=1.
Compute n90=0.
If nghcde=90 n90=1.
Compute n100=0.
If nghcde=100 n100=1.
Compute n102=0.
If nghcde=102 n102=1.
Compute n110=0.
If nghcde=110 n110=1.
Compute n131=0.
If nghcde=131 n131=1.
Compute n132=0.
If nghcde=132 n132=1.
Compute n140=0.
If nghcde=140 n140=1.
Compute n160=0.
If nghcde=160 n160=1.
Compute n180=0.
If nghcde=180 n180=1.
Compute n190=0.
If nghcde=190 n190=1.
Compute n200=0.
If nghcde=200 n200=1.
Compute n210=0.
If nghcde=210 n210=1.
Compute n220=0.
If nghcde=220 n220=1.
Compute n230=0.
If nghcde=230 n230=1.
Compute n250=0.
If nghcde=250 n250=1.
Compute n260=0.
If nghcde=260 n260=1.
Compute n270=0.
If nghcde=270 n270=1.
Compute n280=0.
If nghcde=280 n280=1.
Compute n300=0.
If nghcde=300 n300=1.
Compute n310=0.
If nghcde=310 n310=1.
Compute n320=0.
If nghcde=320 n320=1.
Compute n350=0.
If nghcde=350 n350=1.
Compute n400=0.
If nghcde=400 n400=1.
Compute n420=0.
If nghcde=420 n420=1.

If nghcde=10 N=4.37.
If nghcde=11 N=3.45.
If nghcde=21 N=5.60.
If nghcde=23 N=5.29.
If nghcde=51 N=6.10.
If nghcde=52 N=5.87.
If nghcde=61 N=3.77.
If nghcde=72 N=3.20.
If nghcde=82 N=12.90.
If nghcde=90 N=4.31.
If nghcde=100 N=5.78.
If nghcde=102 N=5.30.
If nghcde=110 N=9.60.
If nghcde=131 N=5.80.
If nghcde=132 N=4.11.
If nghcde=140 N=4.99.
If nghcde=160 N=5.70.
If nghcde=180 N=6.25.
If nghcde=190 N=4.70.
If nghcde=200 N=7.98.
If nghcde=210 N=7.40.
If nghcde=220 N=7.31.
If nghcde=230 N=6.82.
If nghcde=240 N=7.15.
If nghcde=250 N=4.21.
If nghcde=260 N=4.37.
If nghcde=270 N=3.78.
If nghcde=280 N=4.06.
If nghcde=300 N=8.37.
If nghcde=320 N=8.20.
If nghcde=350 N=4.23.
If nghcde=400 N=5.27.
If nghcde=420 N=6.26.

Compute sb10=0.
Compute sb11=0.
Compute sb21=0.
Compute sb23=0.
Compute sb30=0.
Compute sb51=0.
Compute sb52=0.
Compute sb61=0.
Compute sb72=0.
Compute sb82=0.
Compute sb90=0.
Compute sb100=0.
Compute sb102=0.
Compute sb110=0.
Compute sb131=0.
Compute sb132=0.
Compute sb140=0.
Compute sb160=0.
Compute sb180=0.
Compute sb190=0.
Compute sb200=0.
Compute sb210=0.
Compute sb220=0.
Compute sb230=0.
Compute sb250=0.
Compute sb260=0.
Compute sb270=0.
Compute sb280=0.
Compute sb300=0.
Compute sb310=0.
Compute sb320=0.
Compute sb350=0.
Compute sb400=0.
Compute sb420=0.
If nghcde=10 sb10=sqrt(bsf).
If nghcde=11 sb11=sqrt(bsf).
if nghcde=21 sb21=sqrt(bsf).
If nghcde=23 sb23=sqrt(bsf).
If nghcde=30 sb30=sqrt(bsf).
If nghcde=51 sb51=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
If nghcde=61 sb61=sqrt(bsf).
If nghcde=72 sb72=sqrt(bsf).
If nghcde=82 sb82=sqrt(bsf).
if nghcde=90 sb90=sqrt(bsf).
If nghcde=100 sb100=sqrt(bsf).
If nghcde=102 sb102=sqrt(bsf).
If nghcde=110 sb110=sqrt(bsf).
If nghcde=131 sb131=sqrt(bsf).
If nghcde=132 sb132=sqrt(bsf).
If nghcde=140 sb140=sqrt(bsf).
If nghcde=160 sb160=sqrt(bsf).
If nghcde=180 sb180=sqrt(bsf).
If nghcde=190 sb190=sqrt(bsf).
If nghcde=200 sb200=sqrt(bsf).
If nghcde=210 sb210=sqrt(bsf).
If nghcde=220 sb220=sqrt(bsf).
If nghcde=230 sb230=sqrt(bsf).
If nghcde=250 sb250=sqrt(bsf).
If nghcde=260 sb260=sqrt(bsf).
If nghcde=270 sb270=sqrt(bsf).
If nghcde=280 sb280=sqrt(bsf).
If nghcde=300 sb300=sqrt(bsf).
If nghcde=310 sb310=sqrt(bsf).
If nghcde=320 sb320=sqrt(bsf).
If nghcde=350 sb350=sqrt(bsf).
If nghcde=400 sb400=sqrt(bsf).
If nghcde=420 sb420=sqrt(bsf).




Compute lowzonenorthfield=0.
if town=25 and  (nghcde=52 or nghcde=90  or nghcde=131  
or nghcde=220 or nghcde=230 or nghcde=300 or nghcde=310 or nghcde=350 or nghcde=420) lowzonenorthfield=1.                         	

Compute midzonenorthfield=0.
if town=25 and (nghcde=21 or nghcde=23 or nghcde=51 or nghcde=82 or nghcde=100 
or nghcde=102 or nghcde=110 or nghcde=132 or nghcde=140 or nghcde=160 or nghcde=180
or nghcde=190 or nghcde=200 or nghcde=210 or nghcde=240 or nghcde=260 or nghcde=320 
or nghcde=400)   midzonenorthfield=1. 

Compute highzonenorthfield=0.
if town=25 and (nghcde=10 or nghcde=11 or nghcde=61 
or nghcde=72 or nghcde=250 or nghcde=270 or nghcde=280) highzonenorthfield=1.


Compute srfxlowblocknorthfield=0.
if lowzonenorthfield=1 srfxlowblocknorthfield=srfx*lowzonenorthfield.

Compute srfxmidblocknorthfield=0.
if midzonenorthfield=1 srfxmidblocknorthfield=srfx*midzonenorthfield.

Compute srfxhighblocknorthfield=0.
if highzonenorthfield=1 srfxhighblocknorthfield=srfx*highzonenorthfield.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).



Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.



Compute lsf=sqftl.

if class=95 and nghcde=21   lsf=3375.
if class=95 and nghcde=51   lsf=2570.
if class=95 and nghcde=61   lsf=3100.
if class=95 and nghcde=72   lsf=2914.
if class=95 and nghcde=131  lsf=1792.
if class=95 and nghcde=160  lsf=2621.
if class=95 and nghcde=180  lsf=1255.
if class=95 and nghcde=200  lsf=2509.
if class=95 and nghcde=270  lsf=3843.
	

Compute nsrlsf=n*sqrt(lsf).


compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.




Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).

compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (year1 = 2009).
*select if puremarket=1.
*Table observation = b
                amount1
             /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

      

reg des=defaults cov
      /var=amount1 nsrlsf nbsf nbsf234 nbsf56 nbsf778 nbsf1112 nbsf1095 nbsf34 
     nbasfull nbaspart nnobase nfirepl nbsfair nsiteben nrepabsf garage1 garage2
	biggar nbathsum nprembsf nsrage masbsf frabsf stubsf frastbsf srfx
	sb10 sb11 sb21 sb23 sb30 sb51 sb52 sb61 sb72 sb82 sb90 sb100 sb102
	sb110 sb131 sb132 sb140 sb160 sb180 sb190 sb200 sb210 sb220
	sb230 sb250 sb260 sb270 sb280 sb300 sb310 sb320 sb350 sb400 sb420
	jantmar05cl234 winter0506cl234 winter0607cl234 winter0708cl234 winter0809cl234
	summer05cl234 summer06cl234 summer07cl234 summer08cl234 summer09cl234
	octtdec09cl234 jantmar05cl56 winter0506cl56 winter0607cl56 winter0708cl56
	winter0809cl56 summer05cl56 summer06cl56 summer07cl56 summer08cl56
	summer09cl56 octtdec09cl56 jantmar05cl778 winter0506cl778 winter0607cl778
	winter0708cl778 winter0809cl778 summer05cl778 summer06cl778 summer07cl778
    summer08cl778 summer09cl778 octtdec09cl778 jantmar05cl89 winter0506cl89
	winter0607cl89 winter0708cl89 winter0809cl89 summer05cl89 summer06cl89		
	summer07cl89 summer08cl89 summer09cl89 octtdec09cl89 jantmar05cl1112
	winter0506cl1112 winter0607cl1112 winter0708cl1112 winter0809cl1112
	summer05cl1112 summer06cl1112 summer07cl1112 summer08cl1112
	summer09cl1112 octtdec09cl1112 jantmar05cl1095 winter0506cl1095
	winter0607cl1095 winter0708cl1095 winter0809cl1095 summer05cl1095
	summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095
	octtdec09cl1095 jantmar05clsplt winter0506clsplt winter0607clsplt
	winter0708clsplt winter0809clsplt summer05clsplt summer06clsplt
	summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt
     midzonenorthfield highzonenorthfield srfxmidblocknorthfield srfxhighblocknorthfield
	/dep=amount1
   	/method=stepwise
	/method=enter jantmar05cl234 winter0506cl234 winter0607cl234 winter0708cl234 winter0809cl234
	summer05cl234 summer06cl234 summer07cl234 summer08cl234 summer09cl234
	octtdec09cl234 jantmar05cl56 winter0506cl56 winter0607cl56 winter0708cl56
	winter0809cl56 summer05cl56 summer06cl56 summer07cl56 summer08cl56
	summer09cl56 octtdec09cl56 jantmar05cl778 winter0506cl778 winter0607cl778
	winter0708cl778 winter0809cl778 summer05cl778 summer06cl778 summer07cl778
    summer08cl778 summer09cl778 octtdec09cl778 jantmar05cl89 winter0506cl89
	winter0607cl89 winter0708cl89 winter0809cl89 summer05cl89 summer06cl89		
	summer07cl89 summer08cl89 summer09cl89 octtdec09cl89 jantmar05cl1112
	winter0506cl1112 winter0607cl1112 winter0708cl1112 winter0809cl1112
	summer05cl1112 summer06cl1112 summer07cl1112 summer08cl1112
	summer09cl1112 octtdec09cl1112 jantmar05cl1095 winter0506cl1095
	winter0607cl1095 winter0708cl1095 winter0809cl1095 summer05cl1095
	summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095
	octtdec09cl1095 jantmar05clsplt winter0506clsplt winter0607clsplt
	winter0708clsplt winter0809clsplt summer05clsplt summer06clsplt
	summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt
	/method=enter midzonenorthfield highzonenorthfield
      srfxmidblocknorthfield srfxhighblocknorthfield
     /method=enter nbsf234 nbsf1112 nbsf34 frastbsf nbaspart nbsfair nbsf778 nfirepl
	/save pred (pred) resid (resid).
    	sort cases by nghcde pin.	
      	value labels extcon  1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
          /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
          /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
          /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
          /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 
           11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
*compute badsal=0.
*if perdif>50 badsal=1.
*if perdif<-50 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
	/title='Office of the Assessor'
        	'Residential Regression Report'
        	'Town is Northfield'
    	/ltitle 'Report Ran On)Date' 
    	/rtitle='PAGE)PAGE'
    	/string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          	date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhd'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).

