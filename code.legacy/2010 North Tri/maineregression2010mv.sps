                       *SPSS Regression.
	                     *MAINE 2010.

Get file='C:\Program Files\SPSS\spssa\regt22mergefcl2.sav'.
*select if (amount1>190000).
*select if (amount1<950000).
*select if (multi<1).
*select if sqftb<7000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=9 and amount1>0)  year1=2009. 
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  
If  (yr=3 and amount1>0)  year1=2003.
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.



*select if (year1>2004).
set mxcells=2000500.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if(pin=	9173210290000
or pin=	9181020170000
or pin=	9181020400000
or pin=	9181080290000
or pin=	9192070010000
or pin=	9201130120000
or pin=	9072170180000
or pin=	9072190010000
or pin=	9074100480000
or pin=	9331030180000
or pin=	9331030210000
or pin=	9141010060000
or pin=	9141110530000
or pin=	9164010790000
or pin=	9131020340000
or pin=	9131030110000
or pin=	9242130110000
or pin=	9243050510000
or pin=	9244170730000
or pin=	9244230560000
or pin=	9244270030000
or pin=	9134050380000
or pin=	9243210460000
or pin=	9243250440000
or pin=	9112010030000
or pin=	9112060180000
or pin=	9114270130000
or pin=	9121020120000
or pin=	9123010370000
or pin=	9123010730000
or pin=	9123060090000
or pin=	9123130200000
or pin=	9124350020000
or pin=	9143180280000
or pin=	9143200130000
or pin=	9144030430000
or pin=	9144100440000
or pin=	9231022460000
or pin=	9233060200000
or pin=	9294080180000
or pin=	9221150030000
or pin=	9262080010000
or pin=	9221220150000
or pin=	9223020070000
or pin=	9224080490000
or pin=	9261080220000
or pin=	9263030310000
or pin=	9263200400000
or pin=	9271200190000
or pin=	9274040020000
or pin=	9274230230000
or pin=	9233050440000
or pin=	9233100230000
or pin=	9233120280000
or pin=	9233130200000
or pin=	9253020080000
or pin=	9253230180000
or pin=	9264030060000
or pin=	9264040030000
or pin=	9191000830000
or pin=	9191030020000
or pin=	9194060030000
or pin=	9291010100000
or pin=	9304030150000
or pin=	9322000690000
or pin=	9322130240000
or pin=	9344000720000
or pin=	9094010500000
or pin=	9094010680000
or pin=	9212000890000
or pin=	9351000210000
or pin=	9351020280000
or pin=	9351020290000
or pin=	9353030010000
or pin=	9353050030000
or pin=	9353060590000
or pin=	9353110620000
or pin=	9353220260000
or pin=	9353270140000
or pin=	9354040110000
or pin=	9354100050000
or pin=	9354120040000
or pin=	9361000120000
or pin=	9361010020000
or pin=	9361110070000
or pin=	9361110160000
or pin=	9361110160000
or pin=	9363090060000
or pin=	9173130060000
or pin=	9202030210000
or pin=	9211010030000
or pin=	9213030320000)  bs=1.

*select if bs=0.

compute bsf=sqftb.
Compute N=1.


*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile78910.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute tnb=(town*1000) + nghcde.

if (tnb= 22010 ) n=3.65.
if (tnb= 22022 ) n=2.55.
if (tnb= 22023 ) n=2.39.
if (tnb= 22030 ) n=2.25.
if (tnb= 22031 ) n=2.45.
if (tnb= 22040 ) n=2.38.
if (tnb= 22050 ) n=2.45.
if (tnb= 22060 ) n=3.15.
if (tnb= 22070 ) n=3.20.
if (tnb= 22080 ) n=2.81.
if (tnb= 22082 ) n=3.12.
if (tnb= 22085 ) n=3.50.
if (tnb= 22087 ) n=3.60.
if (tnb= 22091 ) n=2.32. 
if (tnb= 22093 ) n=2.76.
if (tnb= 22100 ) n=3.82.
if (tnb= 22110 ) n=4.00.
if (tnb= 22111 ) n=3.10.
if (tnb= 22120 ) n=6.18.
if (tnb= 22130 ) n=2.90.
if (tnb= 22140 ) n=3.75.
if (tnb= 22141 ) n=4.95.
if (tnb= 22142 ) n=6.85.
if (tnb= 22143 ) n=3.83.
if (tnb= 22145 ) n=3.95.
if (tnb= 22150 ) n=4.17.
if (tnb= 22180 ) n=2.85.


compute sb22010=0.
compute sb22022=0.
compute sb22023=0.
compute sb22030=0.
compute sb22031=0.
compute sb22040=0.
compute sb22050=0.
compute sb22060=0.
compute sb22070=0.
compute sb22080=0.
compute sb22082=0.
compute sb22085=0.
compute sb22087=0.
compute sb22091=0.
compute sb22093=0. 
compute sb22100=0.
compute sb22110=0.
compute sb22111=0.
compute sb22112=0.
compute sb22120=0.
compute sb22130=0.
compute sb22140=0.
compute sb22141=0.
compute sb22142=0.
compute sb22143=0.
compute sb22145=0.
compute sb22150=0.
compute sb22180=0.


compute n22010=0.
compute n22022=0.
compute n22023=0.
compute n22030=0.
compute n22031=0.
compute n22040=0.
compute n22050=0.
compute n22060=0.
compute n22070=0.
compute n22080=0.
compute n22082=0.
compute n22085=0.
compute n22087=0.
compute n22091=0.
compute n22093=0.
compute n22100=0. 
compute n22110=0.
compute n22111=0.
compute n22112=0.
compute n22120=0.
compute n22130=0.
compute n22140=0.
compute n22141=0.
compute n22142=0.
compute n22143=0.
compute n22145=0.
compute n22150=0.
compute n22180=0.



if (tnb= 22010 ) n22010=1.
if (tnb= 22022 ) n22022=1.
if (tnb= 22023 ) n22023=1.
if (tnb= 22030 ) n22030=1.
if (tnb= 22031 ) n22031=1.
if (tnb= 22040 ) n22040=1.
if (tnb= 22050 ) n22050=1.
if (tnb= 22060 ) n22060=1.
if (tnb= 22070 ) n22070=1.
if (tnb= 22080 ) n22080=1.
if (tnb= 22082 ) n22082=1.
if (tnb= 22085 ) n22085=1.
if (tnb= 22087 ) n22087=1.
if (tnb= 22091 ) n22091=1.
if (tnb= 22093 ) n22093=1.
if (tnb= 22100 ) n22100=1.
if (tnb= 22110 ) n22110=1.
if (tnb= 22111 ) n22111=1.
if (tnb= 22112 ) n22112=1.
if (tnb= 22120 ) n22120=1.
if (tnb= 22130 ) n22130=1.
if (tnb= 22140 ) n22140=1.
if (tnb= 22141 ) n22141=1.
if (tnb= 22142 ) n22142=1.
if (tnb= 22143 ) n22143=1.
if (tnb= 22145 ) n23145=1.
if (tnb= 22150 ) n22150=1.
if (tnb= 22180 ) n22180=1.



if (tnb= 22010 ) sb22010=sqrt(bsf).
if (tnb= 22022 ) sb22022=sqrt(bsf).
if (tnb= 22023 ) sb22023=sqrt(bsf).
if (tnb= 22030 ) sb22030=sqrt(bsf).
if (tnb= 22031 ) sb22031=sqrt(bsf).
if (tnb= 22040 ) sb22040=sqrt(bsf).
if (tnb= 22050 ) sb22050=sqrt(bsf).
if (tnb= 22060 ) sb22060=sqrt(bsf).
if (tnb= 22070 ) sb22070=sqrt(bsf).
if (tnb= 22080 ) sb22080=sqrt(bsf).
if (tnb= 22082 ) sb22082=sqrt(bsf).
if (tnb= 22085 ) sb22085=sqrt(bsf).
if (tnb= 22087 ) sb22087=sqrt(bsf).
if (tnb= 22091 ) sb22091=sqrt(bsf).
if (tnb= 22093 ) sb22093=sqrt(bsf).
if (tnb= 22100 ) sb22100=sqrt(bsf).
if (tnb= 22110 ) sb22110=sqrt(bsf).
if (tnb= 22111 ) sb22111=sqrt(bsf).
if (tnb= 22112 ) sb22112=sqrt(bsf).
if (tnb= 22120 ) sb22120=sqrt(bsf).
if (tnb= 22130 ) sb22130=sqrt(bsf).
if (tnb= 22140 ) sb22140=sqrt(bsf).
if (tnb= 22141 ) sb22141=sqrt(bsf).
if (tnb= 22142 ) sb22142=sqrt(bsf).
if (tnb= 22143 ) sb22143=sqrt(bsf).
if (tnb= 22145 ) sb23145=sqrt(bsf).
if (tnb= 22150 ) sb22150=sqrt(bsf).
if (tnb= 22180 ) sb22180=sqrt(bsf).




Compute lowzonemaine=0.
if town=22 and  (nghcde=10 or nghcde=110 
or nghcde=120 or nghcde=141 or nghcde=142 or nghcde=150) lowzonemaine=1.                         	

Compute midzonemaine=0.
if town=22 and (nghcde=22 or nghcde=23 or nghcde=30 or nghcde=31 
or nghcde=70 or nghcde=80 or nghcde=85 or nghcde=87 or nghcde=93
or nghcde=111 or nghcde=130 or nghcde=140 or nghcde=143 
or nghcde=180)   midzonemaine=1. 

Compute highzonemaine=0.
if town=22 and (nghcde=40 or nghcde=50 or nghcde=60  
or nghcde=82 or nghcde=91 or nghcde=100 or nghcde=145)   highzonemaine=1.


Compute srfxlowblockmaine=0.
if lowzonemaine=1 srfxlowblockmaine=srfx*lowzonemaine.

Compute srfxmidblockmaine=0.
if midzonemaine=1 srfxmidblockmaine=srfx*midzonemaine.

Compute srfxhighblockmaine=0.
if highzonemaine=1 srfxhighblockmaine=srfx*highzonemaine.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).



Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.



Compute lsf=sqftl.

if class=95 and tnb=22031   lsf=2934.
if class=95 and tnb=22040   lsf=2093.
if class=95 and tnb=22050   lsf=2952.
if class=95 and tnb=22085   lsf=2048.
if class=95 and tnb=22091   lsf=1150.
if class=95 and tnb=22110   lsf=1200.
if class=95 and tnb=22111   lsf=2155.	
if class=95 and tnb=22130   lsf=1788.	
if class=95 and tnb=22180   lsf=2600.


Compute nsrlsf=n*sqrt(lsf).


compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).

If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute mv = (339147.2999	
+ 4006.589026*nbathsum
+ 804.4671846*nbsf1095
+ 949.1703255*nbsf778
+ 6065.144393*nfirepl
+ 1277.885725*sb22150
+ 5.578818018*frastbsf
+ 529.3032217*nsrlsf
- 7504.257781*nsrage
+ 1790.242282*nbsf56
+ 1984.908976*sb22111
- 3087.602983*sb22010
- 83353.2251*lowzonemaine
+ 1728.90325*sb22080
+ 405.5269564*sb22085
+ 2605.838284*sb22100
- 749.119389*sb22120
- 9003.08561*nnobase
+ 6370.933362*garage1
+ 804.847988*sb22070
+ 471.8112333*sb22087
+ 10.38581758*masbsf
+ 1487.342085*sb22050
- 186136.789*highzonemaine
- 132500.353*midzonemaine
+ 1437.949689*sb22040
+ 1418.656398*nbsf234
+ 0.52070563*nbsfair
+ 1324.553608*nbsf34
+ 554.9526445*nbsf1112
+ 1454.580367*sb22091
- 6200.918564*sb22142
- 3082.753863*nbaspart
- 1588.795872*sb22140
- 629.3937763*sb22130
+ 119.352194*nbsf89
+ 28609.09344*biggar
- 4348.938048*srfxlowblockmaine
- 3598.701738*srfxmidblockmaine
- 211.8189401*srfxhighblockmaine
+ 15238.505488*garage2
+ 1.373555182*nprembsf
- 0.912921652*frabsf
+ 2397.5953*nbnum)*.782554.

* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=22.
*sel if puremarket=1 and year1=2009.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\SPSS\spssa\mv22jun30.sav'/keep town pin mv .
