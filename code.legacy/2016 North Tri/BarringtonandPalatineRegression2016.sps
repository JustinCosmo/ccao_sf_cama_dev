                        *SPSS Regression.
			    *BARRINGTON AND PALATINE REGRESSION  2016.


Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regts10and29f.sav'.
select if (amount1>265000).
select if (amount1<1900000).
select if (multi<1).
select if sqftb<9000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=15 and amount1>0)  year1=2015. 
If  (yr=14 and amount1>0)  year1=2014.
If  (yr=13 and amount1>0)  year1=2013.
If (yr=12 and amount1>0)  year1=2012.
If (yr=11 and amount1>0)  year1=2011.
If (yr=10 and amount1>0)  year1=2010.  
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.  
If  (yr=4 and amount1>0)  year1=2004. 

select if (year1>2010).
set mxcells=2500000.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.

if (pin=	1011200010000
or pin=	1011240590000
or pin=	1012020210000
or pin=	1012040020000
or pin=	1012050120000
or pin=	1012090150000
or pin=	1012140080000
or pin=	1012160190000
or pin=	1014010140000
or pin=	1014030400000
or pin=	1024070110000
or pin=	1023000510000
or pin=	1042050020000
or pin=	1061000280000
or pin=	1063000230000
or pin=	1073000180000
or pin=	1091020040000
or pin=	1092000110000
or pin=	1102000080000
or pin=	1181020040000
or pin=	1193010010000
or pin=	1193010260000
or pin=	1212060070000
or pin=	1224010310000
or pin=	1243000110000
or pin=	1273040100000
or pin=	1274020070000
or pin=	1274060020000
or pin=	1352010080000
or pin=	1362010220000
or pin=	1284120060000
or pin=	1242030200000
or pin=	1242030600000
or pin=	1124030220000
or pin=	1124040040000
or pin=	1131030040000
or pin=	1132060020000
or pin=	1132060040000
or pin=	1221040080000
or pin=	1224000310000
or pin=	1341010140000
or pin=	1342000200000
or pin=	2071020030000
or pin=	2072030030000
or pin=	2073040140000
or pin=	2053030040000
or pin=	2062000170000
or pin=	2062030110000
or pin=	2171050050000
or pin=	2083040040000
or pin=	2084050170000
or pin=	2172050170000
or pin=	2202000470000
or pin=	2202010100000
or pin=	2202020030000
or pin=	2202050150000
or pin=	2204010020000
or pin=	2211000070000
or pin=	2292000060000
or pin=	2031060060000
or pin=	2093140080000
or pin=	2154190020000
or pin=	2212130040000
or pin=	2232080140000
or pin=	2224060270000
or pin=	2091190020000
or pin=	2282080060000
or pin=	2364160170000
or pin=	2281110110000
or pin=	2051000030000) bs=1.
select if bs=0.

compute bsf=sqftb.
Compute lsf=sqftl.
Compute N=1.



*COMPUTE FX =  cumfile13141516.
*COMPUTE SRFX = sqrt(fx).
*RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute bar=0.
compute pal=0.
if town=10 bar=1.
if town=29 pal=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000) + nghcde.

if (tnb= 10011 ) n=3.99.
if (tnb= 10012 ) n=4.47.
if (tnb= 10014 ) n=4.99.
if (tnb= 10021 ) n=6.93.
if (tnb= 10022 ) n=4.18.
if (tnb= 10023 ) n=8.33.
if (tnb= 10024 ) n=7.20.
if (tnb= 10025 ) n=5.95.
if (tnb= 10030 ) n=6.35.
if (tnb= 10031 ) n=9.36.
if (tnb= 10040 ) n=3.38.
if (tnb= 29011 ) n=5.65.
if (tnb= 29013 ) n=4.57.
if (tnb= 29022 ) n=4.11.
if (tnb= 29031 ) n=5.10.
if (tnb= 29032 ) n=4.39.
if (tnb= 29033 ) n=3.22.
if (tnb= 29041 ) n=6.48.
if (tnb= 29042 ) n=5.75.
if (tnb= 29043 ) n=3.81.
if (tnb= 29050 ) n=3.61.
if (tnb= 29060 ) n=3.46.
if (tnb= 29070 ) n=3.19.
if (tnb= 29080 ) n=3.44.
if (tnb= 29082 ) n=4.65.
if (tnb= 29083 ) n=4.88.
if (tnb= 29084 ) n=4.53.
if (tnb= 29090 ) n=4.26.
if (tnb= 29100 ) n=3.09.
if (tnb= 29110 ) n=7.07.
if (tnb= 29120 ) n=8.02.
if (tnb= 29130 ) n=7.49.
if (tnb= 29140 ) n=5.45.
if (tnb= 29170 ) n=4.45.
if (tnb= 29180 ) n=4.67.

compute sb10011=0.
compute sb10012=0.
compute sb10014=0.
compute sb10021=0.
compute sb10022=0.
compute sb10023=0.
compute sb10024=0.
compute sb10025=0.
compute sb10030=0.
compute sb10031=0.
compute sb10040=0.
compute sb29011=0.
compute sb29013=0.
compute sb29021=0. 
compute sb29022=0.
compute sb29031=0.
compute sb29032=0.
compute sb29033=0.
compute sb29041=0.
compute sb29042=0.
compute sb29043=0.
compute sb29050=0.
compute sb29060=0.
compute sb29070=0.
compute sb29080=0.
compute sb29082=0.
compute sb29083=0.
compute sb29084=0.
compute sb29090=0.
compute sb29100=0.
compute sb29110=0.
compute sb29120=0.
compute sb29130=0.
compute sb29140=0.
compute sb29160=0.
compute sb29170=0.
compute sb29180=0.


compute n10011=0.
compute n10012=0.
compute n10014=0.
compute n10021=0.
compute n10022=0.
compute n10023=0.
compute n10024=0.
compute n10025=0.
compute n10030=0.
compute n10031=0.
compute n10040=0.
compute n29011=0.
compute n29013=0.
compute n29021=0.
compute n29022=0. 
compute n29031=0.
compute n29032=0.
compute n29033=0.
compute n29041=0.
compute n29042=0.
compute n29043=0.
compute n29050=0.
compute n29060=0.
compute n29070=0.
compute n29080=0.
compute n29082=0.
compute n29083=0.
compute n29084=0.
compute n29090=0.
compute n29100=0.
compute n29110=0.
compute n29120=0.
compute n29130=0.
compute n29140=0.
compute n29160=0.
compute n29170=0.
compute n29180=0.


if (tnb= 10011 )  n10011=1.
if (tnb= 10012 )  n10012=1.
if (tnb= 10014 )  n10014=1.
if (tnb= 10021 )  n10021=1.
if (tnb= 10022 )  n10022=1.
if (tnb= 10023 )  n10023=1.
if (tnb= 10024 )  n10024=1.
if (tnb= 10025 )  n10025=1.
if (tnb= 10030 )  n10030=1.
if (tnb= 10031 )  n10031=1.
if (tnb= 10040 )  n10040=1.
if (tnb= 29011 )  n29011=1.
if (tnb= 29013 )  n29013=1.
if (tnb= 29021 )  n29021=1.
if (tnb= 29022 )  n29022=1.
if (tnb= 29031 )  n29031=1.
if (tnb= 29032 )  n29032=1.
if (tnb= 29033 )  n29033=1.
if (tnb= 29041 )  n29041=1.
if (tnb= 29042 )  n29042=1.
if (tnb= 29043 )  n29043=1.
if (tnb= 29050 )  n29050=1.
if (tnb= 29060 )  n29060=1.
if (tnb= 29070 )  n29070=1.
if (tnb= 29080 )  n29080=1.
if (tnb= 29082 )  n29082=1.
if (tnb= 29083 )  n29083=1.
if (tnb= 29084 )  n29084=1.
if (tnb= 29090 )  n29090=1.
if (tnb= 29100 )  n29100=1.
if (tnb= 29110 )  n29110=1.
if (tnb= 29120 )  n29120=1.
if (tnb= 29130 )  n29130=1.
if (tnb= 29140 )  n29140=1.
if (tnb= 29160 )  n29160=1.
if (tnb= 29170 )  n29170=1.
if (tnb= 29180 )  n29180=1.

if (tnb= 10011 ) sb10011=sqrt(bsf).
if (tnb= 10012 ) sb10012=sqrt(bsf).
if (tnb= 10014 ) sb10014=sqrt(bsf).
if (tnb= 10021 ) sb10021=sqrt(bsf).
if (tnb= 10022 ) sb10022=sqrt(bsf).
if (tnb= 10023 ) sb10023=sqrt(bsf).
if (tnb= 10024 ) sb10024=sqrt(bsf).
if (tnb= 10025 ) sb10025=sqrt(bsf).
if (tnb= 10030 ) sb10030=sqrt(bsf).
if (tnb= 10031 ) sb10031=sqrt(bsf).
if (tnb= 10040 ) sb10040=sqrt(bsf).
if (tnb= 29011 ) sb29011=sqrt(bsf).
if (tnb= 29013 ) sb29013=sqrt(bsf).
if (tnb= 29022 ) sb29022=sqrt(bsf).
if (tnb= 29031 ) sb29031=sqrt(bsf).
if (tnb= 29032 ) sb29032=sqrt(bsf).
if (tnb= 29033 ) sb29033=sqrt(bsf).
if (tnb= 29041 ) sb29041=sqrt(bsf).
if (tnb= 29042 ) sb29042=sqrt(bsf).
if (tnb= 29043)  sb29043=sqrt(bsf).
if (tnb= 29050 ) sb29050=sqrt(bsf).
if (tnb= 29060 ) sb29060=sqrt(bsf).
if (tnb= 29070 ) sb29070=sqrt(bsf).
if (tnb= 29080 ) sb29080=sqrt(bsf).
if (tnb= 29082 ) sb29082=sqrt(bsf).
if (tnb= 29083 ) sb29083=sqrt(bsf).
if (tnb= 29084 ) sb29084=sqrt(bsf).
if (tnb= 29090 ) sb29090=sqrt(bsf).
if (tnb= 29100 ) sb29100=sqrt(bsf).
if (tnb= 29110 ) sb29110=sqrt(bsf).
if (tnb= 29120 ) sb29120=sqrt(bsf).
if (tnb= 29130 ) sb29130=sqrt(bsf).
if (tnb= 29140 ) sb29140=sqrt(bsf).
if (tnb= 29170 ) sb29170=sqrt(bsf).
if (tnb= 29180 ) sb29180=sqrt(bsf).



*Compute lowzonebar=0.
*if town=10 and  (nghcde=12 or nghcde=14 or nghcde=21 or nghcde=22
 or nghcde=23  or nghcde=24 or nghcde=30 or nghcde=31 or nbhcde=40) lowzonebar=1.                         	

*Compute midzonebar=0.
*if town=10 and (nghcde=11 or nghcde=25) midzonebar=1.

*Compute srfxlowblockbar=0.
*if lowzonebar=1 srfxlowblockbar=srfx*lowzonebar.

*Compute srfxmidblockbar=0.
*if midzonebar=1 srfxmidblockbar=srfx*midzonebar.

*Compute midzonepal=0.
*if town=29 and (nghcde=22 or nghcde=50 or nghcde=100) midzonepal=1.

*Compute lowzonepal=0.
*if (town=29  and  midzonepal=1)   lowzonepal=1.  
                  	
*Compute srfxlowblockpal=0.
*if lowzonepal=1 srfxlowblockpal=srfx*lowzonepal.

*Compute srfxmidblockpal=0.
*if midzonepal=1 srfxmidblockpal=srfx*midzonepal.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1112=0.
if (mos > 9 and yr=11 or (mos <= 3 and yr=12)) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1. 
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute jantmar11=0.
if (year1=2011 and (mos>=1 and mos<=3)) jantmar11=1. 
Compute octtdec15=0.
if (year1=2015 and (mos>=10 and mos<=12)) octtdec15=1.

Compute jantmar11cl234=jantmar11*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute octtdec15cl234=octtdec15*cl234.

Compute jantmar11cl56=jantmar11*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute octtdec15cl56=octtdec15*cl56.

Compute jantmar11cl778=jantmar11*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute octtdec15cl778=octtdec15*cl778.

Compute jantmar11cl89=jantmar11*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute octtdec15cl89=octtdec15*cl89.


Compute jantmar11cl1112=jantmar11*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute octtdec15cl1112=octtdec15*cl1112.

Compute jantmar11cl1095=jantmar11*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute octtdec15cl1095=octtdec15*cl1095.

Compute jantmar11clsplt=jantmar11*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute octtdec15clsplt=octtdec15*clsplt.



if class=95 and tnb=10031  	lsf=7375.
if class=95 and tnb=29021  	lsf=2340.
if class=95 and tnb=29022  	lsf=2665.
if class=95 and tnb=29033  	lsf=3298.
if class=95 and tnb=29043  	lsf=2988.
if class=95 and tnb=29050  	lsf=1179.
if class=95 and tnb=29060  	lsf=2244.
if class=95 and tnb=29070  	lsf=1760.
if class=95 and tnb=29080  	lsf=2516.
if class=95 and tnb=29084  	lsf=2630.
if class=95 and tnb=29090  	lsf=2887.
if class=95 and tnb=29160 	lsf=2593.
if class=95 and tnb=29170 	lsf=2340.


compute nsrpall=0.
compute nsrbarl=0.
if pal=1 nsrpall=n*pal*sqrt(lsf).
if bar=1 nsrbarl=n*bar*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrpalb=n*pal*sqrt(bsf).
Compute nsrbarb=n*bar*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (town=10 and year1=2015) or (town=29 and year1=2015).
*select if puremarket=1. 
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').


reg des=defaults cov
  /var=amount1 nsrpall nsrbarl  nbsf234
	 nbsf56 nbsf778 nbsf1112 nbsf1095 nbsf34 nbsf89 nbasfull nbaspart nfirepl nbsfair
	 nsiteben nluxbsf garnogar garage1 garage2 biggar nnobase nbathsum nprembsf nsrage 
	 sb10011 sb10012 sb10014 sb10021 sb10022
	 sb10023 sb10024 sb10025 sb10030 sb10031 sb10040 sb29011 sb29013 sb29022 sb29031 
	 sb29032 sb29033 sb29041 sb29042 sb29043 sb29050 sb29060 sb29070 sb29080 sb29082
	 sb29083 sb29084 sb29090 sb29100 sb29110 sb29120 sb29130 sb29140 sb29170 sb29180 
	   jantmar11cl234 winter1112cl234 winter1213cl234 winter1314cl234 winter1415cl234 summer11cl234
   summer12cl234 summer13cl234 summer14cl234 summer15cl234 octtdec15cl234 jantmar11cl56
   winter1112cl56 winter1213cl56 winter1314cl56 winter1415cl56 summer11cl56 summer12cl56
   summer13cl56 summer14cl56 summer15cl56 octtdec15cl56  jantmar11cl778 winter1112cl778
   winter1213cl778 winter1314cl778 winter1415cl778 summer11cl778 summer12cl778 summer13cl778
   summer14cl778 summer15cl778 octtdec15cl778 jantmar11cl89 winter1112cl89 winter1213cl89
   winter1314cl89 winter1415cl89 summer11cl89 summer12cl89 summer13cl89 summer14cl89
   summer15cl89 octtdec15cl89 jantmar11cl1112 winter1112cl1112 winter1213cl1112 winter1314cl1112 
   winter1415cl1112 summer11cl1112 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112
   octtdec15cl1112 jantmar11cl1095 winter1112cl1095 winter1213cl1095 winter1314cl1095 winter1415cl1095
   summer11cl1095 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 octtdec15cl1095
   jantmar11clsplt winter1112clsplt winter1213clsplt winter1314clsplt winter1415clsplt summer11clsplt summer12clsplt
   summer13clsplt summer14clsplt summer15clsplt octtdec15clsplt 
	 /dep=amount1
  /method=stepwise 
	 /method= enter nbsf234 nbsf778 nbsf34 nbsf89  garage1 garage2 biggar 
	 /method=enter    jantmar11cl234 winter1112cl234 winter1213cl234 winter1314cl234 winter1415cl234 summer11cl234
   summer12cl234 summer13cl234 summer14cl234 summer15cl234 octtdec15cl234 jantmar11cl56
   winter1112cl56 winter1213cl56 winter1314cl56 winter1415cl56 summer11cl56 summer12cl56
   summer13cl56 summer14cl56 summer15cl56 octtdec15cl56  jantmar11cl778 winter1112cl778
   winter1213cl778 winter1314cl778 winter1415cl778 summer11cl778 summer12cl778 summer13cl778
   summer14cl778 summer15cl778 octtdec15cl778 jantmar11cl89 winter1112cl89 winter1213cl89
   winter1314cl89 winter1415cl89 summer11cl89 summer12cl89 summer13cl89 summer14cl89
   summer15cl89 octtdec15cl89 jantmar11cl1112 winter1112cl1112 winter1213cl1112 winter1314cl1112 
   winter1415cl1112 summer11cl1112 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112
   octtdec15cl1112 jantmar11cl1095 winter1112cl1095 winter1213cl1095 winter1314cl1095 winter1415cl1095
   summer11cl1095 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 octtdec15cl1095
   jantmar11clsplt winter1112clsplt winter1213clsplt winter1314clsplt winter1415clsplt summer11clsplt summer12clsplt
   summer13clsplt summer14clsplt summer15clsplt octtdec15clsplt 
  /method=enter  nsrpall nsrbarl nfirepl nbaspart nnobase sb10011 sb10021 sb10022 sb29013 sb29090 
  /method=enter  nbsfair garage1 garnogar 
  /method=enter nprembsf
	 /save pred (pred) resid (resid).
    	 sort cases by tnb pin.	
    	 value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
    	 /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
    	 /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
    	 /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
    	 /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
compute perdif=(resid)/(amount1)*100.
formats pred(COMMA9.0)
          /resid (f6.0).
exe.
*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
compute badsal=0.
if perdif>50 badsal=1.
if perdif<-50 badsal=1.
select if badsal=1.
exe.
set wid=125.
set len=65.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Evanston'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'year1(4))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       YEAR1 'YR'(4)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
 	



 	

 
