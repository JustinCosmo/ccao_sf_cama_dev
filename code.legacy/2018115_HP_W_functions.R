#S3 print method for the class    
print.money <- function(x, ...) {
  print.default(paste0("$", formatC(as.numeric(x), format="f", digits=2, big.mark=",")))
}

#format method, which is necessary for formating in a data.frame   
format.money  <- function(x, ...) {
  paste0("$", formatC(as.numeric(x), format="f", digits=2, big.mark=","))
}

# This doesn't work
killDbConnections <- function () {
  
  all_cons <- dbListConnections(RPostgreSQL())
  
  print(all_cons)
  
  for(con in all_cons)
    +  dbDisconnect(con)
  
  print(paste(length(all_cons), " connections killed."))
  
}


df = data.frame(x=1:100)
df$y = sin(df$x)

ggplot(df, aes(x=x, y=y)) + 
labs(x="Trim iteration", y="Y") 
