                                  *SPSS Regression.
				    *Hyde Park and South Multiple Regression 2009.


Get file='C:\Program Files\SPSS\spssa\regt70andregt76mergefcl3.sav'.
select if (amount1>75000).
select if (amount1<600000).
select if (multi<1).
select if sqftb<9000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.   
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.
select if (year1>2003).

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	20021060170000
or pin=	20021110160000
or pin=	20021130120000
or pin=	20021200220000
or pin=	20023020190000
or pin=	20024000440000
or pin=	20024000530000
or pin=	20031030210000
or pin=	20031220100000
or pin=	20032010210000
or pin=	20032130290000
or pin=	20032170060000
or pin=	20032180520000
or pin=	20032190340000
or pin=	20032200270000
or pin=	20032240180000
or pin=	20033030100000
or pin=	20033100040000
or pin=	20033100220000
or pin=	20033130430000
or pin=	20033140370000
or pin=	20033220120000
or pin=	20034000520000
or pin=	20034030310000
or pin=	20034030320000
or pin=	20034080220000
or pin=	20034140150000
or pin=	20034170100000
or pin=	20034180200000
or pin=	20034230190000
or pin=	20034270270000
or pin=	20101020150000
or pin=	20101030060000
or pin=	20101090200000
or pin=	20102110290000
or pin=	20102130330000
or pin=	20102150230000
or pin=	20102170220000
or pin=	20102180290000
or pin=	20102180330000
or pin=	20103040270000
or pin=	20103070150000
or pin=	20103120060000
or pin=	20111050300000
or pin=	20111050490000
or pin=	20111050510000
or pin=	20111050750000
or pin=	20151060220000
or pin=	20151080120000
or pin=	20151080270000
or pin=	20151120060000
or pin=	20151150280000
or pin=	20151160100000
or pin=	20151190250000
or pin=	20151220250000
or pin=	20153040070000
or pin=	20153070190000
or pin=	20153090050000
or pin=	20153130610000
or pin=	20111140340000
or pin=	20111140380000
or pin=	20111140490000
or pin=	20111140540000
or pin=	20113000200000
or pin=	20113120210000
or pin=	20114230020000
or pin=	20114230530000
or pin=	20154080160000
or pin=	20154090210000
or pin=	20154120180000
or pin=	20154130200000
or pin=	20154200200000
or pin=	20154220180000
or pin=	20221050540000
or pin=	20221050700000
or pin=	20221070110000
or pin=	20221070490000
or pin=	20222050080000
or pin=	20222060170000
or pin=	20222140100000
or pin=	20222180250000
or pin=	20222180360000
or pin=	20222290200000
or pin=	20223010450000
or pin=	20223030290000
or pin=	20223040470000
or pin=	20223070470000
or pin=	20223090370000
or pin=	20223100380000
or pin=	20223110280000
or pin=	20223130130000
or pin=	20223170040000
or pin=	20223170090000
or pin=	20223260030000
or pin=	20224010070000
or pin=	20224010200000
or pin=	20224020470000
or pin=	20224100180000
or pin=	20224140160000
or pin=	20224180050000
or pin=	20224180550000
or pin=	20224210190000
or pin=	20224210260000
or pin=	20224210340000
or pin=	20224220200000
or pin=	20271000330000
or pin=	20271080120000
or pin=	20271170280000
or pin=	20272010080000
or pin=	20272050370000
or pin=	20272110300000
or pin=	20272130360000
or pin=	20272150100000
or pin=	20272270130000
or pin=	20272300180000
or pin=	20274040050000
or pin=	20274040300000
or pin=	20274040370000
or pin=	20274110110000
or pin=	20274110160000
or pin=	20274210380000
or pin=	20274220310000
or pin=	20143080160000
or pin=	20144120260000
or pin=	20231130090000
or pin=	20231190070000
or pin=	20231210310000
or pin=	20231230120000
or pin=	20231240170000
or pin=	20231250190000
or pin=	20231260120000
or pin=	20231260320000
or pin=	20234060120000
or pin=	20234060240000
or pin=	20234080030000
or pin=	20234110250000
or pin=	20234120330000
or pin=	20234160200000
or pin=	20234190110000
or pin=	20234240260000
or pin=	20234240340000
or pin=	20261030040000
or pin=	20261040040000
or pin=	20261050050000
or pin=	20261060340000
or pin=	20261210030000
or pin=	20261210050000
or pin=	20262020040000
or pin=	20262070200000
or pin=	20262180190000
or pin=	20262260290000
or pin=	20263040150000
or pin=	20263090090000
or pin=	20263100200000
or pin=	20263120120000
or pin=	20263160340000
or pin=	20263180040000
or pin=	20263190110000
or pin=	20263220300000
or pin=	20264030240000
or pin=	20264030350000
or pin=	20264040140000
or pin=	20251130250000
or pin=	20251140040000
or pin=	20253020110000
or pin=	20253100070000
or pin=	20254030130000
or pin=	20254040250000
or pin=	20254170220000
or pin=	20254170330000
or pin=	20254220270000
or pin=	20351241770000
or pin=	20352090170000
or pin=	20352120140000
or pin=	20352190310000
or pin=	20352230100000
or pin=	20354070070000
or pin=	20361020210000
or pin=	20361020230000
or pin=	20361080330000
or pin=	20361080350000
or pin=	20362070410000
or pin=	20362160100000
or pin=	20362200060000
or pin=	20363020190000
or pin=	20363280200000
or pin=	20363290580000
or pin=	20364010210000
or pin=	20364020290000
or pin=	20364020340000
or pin=	20364140090000
or pin=	21301120380000
or pin=	21301150060000
or pin=	21301220430000
or pin=	21302000130000
or pin=	21303000140000
or pin=	21303160070000
or pin=	21304020380000
or pin=	21304040410000
or pin=	21304110210000
or pin=	21304140460000
or pin=	21304140660000
or pin=	21304150300000
or pin=	25022130230000
or pin=	25022180120000
or pin=	25024040010000
or pin=	25024120320000
or pin=	25024120400000
or pin=	20252040050000
or pin=	20252080070000
or pin=	20252130130000
or pin=	20252150050000
or pin=	20252180230000
or pin=	20252200140000
or pin=	20252230120000
or pin=	20252280130000
or pin=	20252280190000
or pin=	20252280360000
or pin=	20353190340000
or pin=	20353200240000
or pin=	21303120260000
or pin=	21303230190000
or pin=	21303240180000
or pin=	21303310120000
or pin=	21311030020000
or pin=	21311220120000
or pin=	21311280220000
or pin=	21312120410000
or pin=	21312170010000
or pin=	21312240310000
or pin=	21313020370000
or pin=	21313230170000
or pin=	21313230240000
or pin=	21313290120000
or pin=	21314180040000
or pin=	21314270050000
or pin=	21314270160000
or pin=	26062000020000
or pin=	26062010260000
or pin=	26062010300000
or pin=	26062020270000
or pin=	26062090060000
or pin=	26062090070000
or pin=	26062150070000
or pin=	26062150400000
or pin=	26062160360000
or pin=	26081150480000
or pin=	26081220010000
or pin=	20273040390000
or pin=	20273150140000
or pin=	20273160270000
or pin=	20341020270000
or pin=	20341110070000
or pin=	20341180010000
or pin=	20341200460000
or pin=	20343000360000
or pin=	20343030170000
or pin=	20343060300000
or pin=	20343150250000
or pin=	20343160240000
or pin=	25031060370000
or pin=	25031180190000
or pin=	25031210180000
or pin=	25033030110000
or pin=	25033040210000
or pin=	25033120510000
or pin=	25033130720000
or pin=	25033190360000
or pin=	25034290020000
or pin=	25101020290000
or pin=	25101030310000
or pin=	20342230050000
or pin=	20344110110000
or pin=	20351000050000
or pin=	20351080070000
or pin=	20351150070000
or pin=	20351220450000
or pin=	20351230170000
or pin=	20353020180000
or pin=	20353100570000
or pin=	20353110570000
or pin=	25032040250000
or pin=	25032110190000
or pin=	25032220060000
or pin=	25032260120000
or pin=	25034060010000
or pin=	25034100360000
or pin=	25034110260000
or pin=	25034110310000
or pin=	25034110360000
or pin=	25034160140000
or pin=	25034230120000
or pin=	25034310290000
or pin=	25021050480000
or pin=	25023090250000
or pin=	25023140090000
or pin=	25023170380000
or pin=	25024080160000
or pin=	25024150190000
or pin=	25011010300000
or pin=	25011110180000
or pin=	25011230330000
or pin=	25011320220000
or pin=	25012170140000
or pin=	25014110060000
or pin=	25014140470000
or pin=	25014210090000
or pin=	26061230560000
or pin=	26061310230000
or pin=	26063060070000
or pin=	26063100110000
or pin=	26064140080000
or pin=	26064150080000
or pin=	26071170450000
or pin=	26071190200000
or pin=	26071200410000
or pin=	21322040480000
or pin=	21322050140000
or pin=	26051020430000
or pin=	26062030240000
or pin=	26062100450000
or pin=	26062170360000
or pin=	26064040160000
or pin=	26064050300000
or pin=	20243140050000
or pin=	20243150100000
or pin=	20243200140000
or pin=	25013000490000
or pin=	25112020240000
or pin=	25112050140000
or pin=	25132070120000
or pin=	25132080290000
or pin=	26071330710000
or pin=	26071450680000
or pin=	26071660060000
or pin=	26073060060000
or pin=	25151020240000
or pin=	25153100030000
or pin=	25153110010000
or pin=	25153180020000
or pin=	25221100230000
or pin=	25221110010000
or pin=	25221150410000
or pin=	25221160360000
or pin=	25221230040000
or pin=	25223080060000
or pin=	25223150120000
or pin=	25271170070000
or pin=	25103060030000
or pin=	25151280090000
or pin=	25153140410000
or pin=	25152220410000
or pin=	25222090270000
or pin=	25222150060000
or pin=	26083050050000
or pin=	26083100280000
or pin=	26083110200000
or pin=	26083210190000
or pin=	26083230480000
or pin=	26182040100000
or pin=	26182040110000
or pin=	26182110250000
or pin=	26201150220000
or pin=	25341190220000
or pin=	25343010020000
or pin=	26311150320000
or pin=	26312250420000
or pin=	17223150390000
or pin=	17221100790000
or pin=	17214320050000
or pin=	17281090610000
or pin=	17281270080000
or pin=	17282090470000
or pin=	17283070200000
or pin=	17283070220000
or pin=	17283090280000
or pin=	17283100080000
or pin=	17283110130000
or pin=	17283150050000
or pin=	17283150350000
or pin=	17283160060000
or pin=	17283210370000
or pin=	17283220190000
or pin=	17283240340000
or pin=	17283270120000
or pin=	17283280380000
or pin=	17283320200000
or pin=	17284000300000
or pin=	17284030020000
or pin=	17331000370000
or pin=	17331090100000
or pin=	17331090490000
or pin=	17331130320000
or pin=	17331210240000
or pin=	17331220500000
or pin=	17331230510000
or pin=	17332030570000
or pin=	17332100100000
or pin=	17333000110000
or pin=	17333220280000
or pin=	17333250350000
or pin=	17341220220000
or pin=	17343090090000
or pin=	17343090290000
or pin=	17343090680000
or pin=	17343120030000
or pin=	17343120640000
or pin=	17343270010000
or pin=	17343280260000
or pin=	17343280300000
or pin=	17343280410000
or pin=	17342191290000
or pin=	17293050060000
or pin=	17293060040000
or pin=	17293130150000
or pin=	17293230330000
or pin=	17293270190000
or pin=	17294110160000
or pin=	17294130130000
or pin=	17294190220000
or pin=	17294200360000
or pin=	17294220150000
or pin=	17294220740000
or pin=	17294230160000
or pin=	17294250650000
or pin=	17321030510000
or pin=	17322030140000
or pin=	17322030360000
or pin=	17322070380000
or pin=	17322170200000
or pin=	17322170960000
or pin=	17322230330000
or pin=	17322270210000
or pin=	17324130170000
or pin=	16354080090000
or pin=	16354140110000
or pin=	16363030160000
or pin=	16363040490000
or pin=	16363170010000
or pin=	16363230110000
or pin=	16364070070000
or pin=	16364090200000
or pin=	16364170300000
or pin=	16364220260000
or pin=	17312180390000
or pin=	17312230250000
or pin=	17313070480000
or pin=	17313080110000
or pin=	17314000400000
or pin=	17314060090000
or pin=	17314300070000
or pin=	17321110240000	) bs=1.
select if bs=0.

Compute aos=12*(2008-year1) + (13-mos).
compute bsf=sqftb.
Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute apt=0.
If class=11 or class=12 apt=1.
Compute aptbsf=apt*bsf.
Compute naptbsf=n*aptbsf.

compute bsf02=bsf*class02.
compute bsf03=bsf*class03.
compute bsf04=bsf*class04.
compute bsf05=bsf*class05.
compute bsf06=bsf*class06.
compute bsf07=bsf*class07.
compute bsf08=bsf*class08.
compute bsf09=bsf*class09.
compute bsf10=bsf*class10.
compute bsf11=bsf*class11.
compute bsf12=bsf*class12.
compute bsf34=bsf*class34.
compute bsf78=bsf*class78.
compute bsf95=bsf*class95.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
compute nbsf1095=n*cl1095*sqrt(bsf).


compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

compute cl95=0.
if class=95 cl95=1.                  	
compute nbsf95=n*cl95*sqrt(bsf).

*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile789.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if puresale=1 or (yr=4 or yr=5).


****************************************************************************************************************.
* The next section computes a  mid-frequency foreclosure zone .
* and a high frequency foreclosure zone based on foreclosure rates. 
* in specific Hyde Park and South neighborhoods (see Hyde Park and South foreclosure maps).
********************************************************************************************.

Compute lowzonehydepark=0.
if town=70 and (nghcde=20 or nghcde=91 or nghcde=101  or nghcde=150  or nghcde=151
or nghcde=240 or nghcde=241  or nghcde=280) lowzonehydepark=1.                         	

Compute midzonehydepark=0.
if town=70 and (nghcde=80 or nghcde=83  or nghcde=111  or nghcde=120 or  nghcde=130
or nghcde=170 or nghcde=180 or nghcde=220 or nghcde=230 or nghcde=250 or nghcde=260) midzonehydepark=1. 

Compute highzonehydepark=0.
if town=70 and (nghcde=10 or nghcde=30  or nghcde=70  or nghcde=100   or nghcde=121
or nghcde=140 or nghcde=210 ) highzonehydepark=1.


Compute srfxlowblockhydepark=0.
if lowzonehydepark=1 srfxlowblockhydepark=srfx*lowzonehydepark.

Compute srfxmidblockhydepark=0.
if midzonehydepark=1 srfxmidblockhydepark=srfx*midzonehydepark.

Compute srfxhighblockhydepark=0.
if highzonehydepark=1 srfxhighblockhydepark=srfx*highzonehydepark.


Compute lowzonesouth=0.
if town=76 and (nghcde=10 or nghcde=11 or nghcde=12  or nghcde=30  
or nghcde=42) lowzonesouth=1. 

Compute midzonesouth=0.
if town=76 and (nghcde=50  or nghcde=60)  midzonesouth=1.   

Compute highzonesouth=0.
if town=76 and (nghcde=40  or nghcde=41 )  highzonesouth=1.


Compute srfxlowblocksouth=0.
if lowzonesouth=1 srfxlowblocksouth=srfx*lowzonesouth.

Compute srfxmidblocksouth=0.
if midzonesouth=1 srfxmidblocksouth=srfx*midzonesouth.

Compute srfxhighblocksouth=0.
if highzonesouth=1 srfxhighblocksouth=srfx*highzonesouth.




*********************************************************************************************.

Compute winter0405=0.
if (mos > 9 and yr=4) or (mos <= 3 and yr=5) winter0405=1.
Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute summer04=0.
if (mos > 3 and yr=4) and (mos <= 9 and yr=4) summer04=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1. 
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute jantmar04=0.
if (year1=2004 and (mos>=1 and mos<=3)) jantmar04=1. 
Compute octtdec08=0.
if (year1=2008 and (mos>=10 and mos<=12)) octtdec08=1.

Compute jantmar04cl234=jantmar04*cl234.
Compute winter0405cl234=winter0405*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute summer04cl234=summer04*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute octtdec08cl234=octtdec08*cl234.

Compute jantmar04cl56=jantmar04*cl56.
Compute winter0405cl56=winter0405*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute summer04cl56=summer04*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute octtdec08cl56=octtdec08*cl56.

Compute jantmar04cl778=jantmar04*cl778.
Compute winter0405cl778=winter0405*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute summer04cl778=summer04*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute octtdec08cl778=octtdec08*cl778.

Compute jantmar04cl89=jantmar04*cl89.
Compute winter0405cl89=winter0405*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute summer04cl89=summer04*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute octtdec08cl89=octtdec08*cl89.

Compute jantmar04cl1112=jantmar04*cl1112. 
Compute winter0405cl1112=winter0405*cl1112.
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute summer04cl1112=summer04*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute octtdec08cl1112=octtdec08*cl1112.

Compute jantmar04cl1095=jantmar04*cl1095.
Compute winter0405cl1095=winter0405*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute summer04cl1095=summer04*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute octtdec08cl1095=octtdec08*cl1095.

Compute jantmar04clsplt=jantmar04*clsplt.
Compute winter0405clsplt=winter0405*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute summer04clsplt=summer04*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute octtdec08clsplt=octtdec08*clsplt.

compute tnb=(town*1000) + nghcde.

If (tnb=70010) n=2.95. 
If (tnb=70020) n=4.40.
If (tnb=70030) n=2.25.
If (tnb=70070) n=2.00. 
If (tnb=70080) n=1.80. 
If (tnb=70083) n=3.22. 
If (tnb=70091) n=1.70. 
If (tnb=70100) n=1.70. 
If (tnb=70101) n=1.50.
If (tnb=70111) n=1.72. 
If (tnb=70120) n=1.65. 
If (tnb=70121) n=1.51. 
If (tnb=70130) n=1.65. 
If (tnb=70140) n=1.73. 
If (tnb=70150) n=4.72.
If (tnb=70151) n=2.32. 
If (tnb=70170) n=1.42. 
If (tnb=70180) n=1.25. 
If (tnb=70210) n=1.80. 
If (tnb=70220) n=1.60. 
If (tnb=70230) n=1.53. 
If (tnb=70240) n=1.45. 
If (tnb=70241) n=2.53. 
If (tnb=70250) n=1.48. 
If (tnb=70260) n=1.00.
If (tnb=70280) n=1.58.
if (tnb=76010) n=5.25. 
if (tnb=76011) n=4.53.
if (tnb=76012) n=5.20.
if (tnb=76030) n=3.50.
if (tnb=76040) n=1.80.
if (tnb=76041) n=2.43.
if (tnb=76042) n=3.14.
if (tnb=76050) n=3.10.
if (tnb=76060) n=2.42.


compute sb70010=0.
compute sb70020=0.
compute sb70030=0.
compute sb70070=0.
compute sb70080=0.
compute sb70083=0.
compute sb70091=0.
compute sb70100=0.
compute sb70101=0.
compute sb70111=0.
compute sb70120=0.
compute sb70121=0.
compute sb70130=0.
compute sb70140=0.
compute sb70150=0.
compute sb70151=0.
compute sb70170=0.
compute sb70180=0.
compute sb70210=0.
compute sb70220=0.
compute sb70230=0.
compute sb70240=0.
compute sb70241=0.
compute sb70250=0.
compute sb70260=0.
compute sb70280=0.
compute sb70300=0.
compute sb76010=0.
compute sb76011=0.
compute sb76012=0.
compute sb76030=0.
compute sb76040=0.
compute sb76041=0.
compute sb76042=0.
compute sb76050=0.
compute sb76060=0.


if (tnb=70010) sb70010=sqrt(bsf).
if (tnb=70020) sb70020=sqrt(bsf).
if (tnb=70030) sb70030=sqrt(bsf).
if (tnb=70070) sb70070=sqrt(bsf).
if (tnb=70080) sb70080=sqrt(bsf).
if (tnb=70083) sb70083=sqrt(bsf).
if (tnb=70091) sb70091=sqrt(bsf).
if (tnb=70100) sb70100=sqrt(bsf).
if (tnb=70101) sb70101=sqrt(bsf).
if (tnb=70111) sb70111=sqrt(bsf).
if (tnb=70120) sb70120=sqrt(bsf).
if (tnb=70121) sb70121=sqrt(bsf).
if (tnb=70130) sb70130=sqrt(bsf).
if (tnb=70140) sb70140=sqrt(bsf).
if (tnb=70150) sb70150=sqrt(bsf).
if (tnb=70151) sb70151=sqrt(bsf).
if (tnb=70170) sb70170=sqrt(bsf).
if (tnb=70180) sb70180=sqrt(bsf).
if (tnb=70210) sb70210=sqrt(bsf).
if (tnb=70220) sb70220=sqrt(bsf).
if (tnb=70230) sb70230=sqrt(bsf).
if (tnb=70240) sb70240=sqrt(bsf).
if (tnb=70241) sb70241=sqrt(bsf).
if (tnb=70250) sb70250=sqrt(bsf).
if (tnb=70260) sb70260=sqrt(bsf).
if (tnb=70280) sb70280=sqrt(bsf).
if (tnb=76010) sb76010=sqrt(bsf).
if (tnb=76011) sb76011=sqrt(bsf).
if (tnb=76012) sb76012=sqrt(bsf).
if (tnb=76030) sb76030=sqrt(bsf).
if (tnb=76040) sb76040=sqrt(bsf).
if (tnb=76041) sb76041=sqrt(bsf).
if (tnb=76042) sb76042=sqrt(bsf).
if (tnb=76050) sb76050=sqrt(bsf).
if (tnb=76060) sb76060=sqrt(bsf).

compute n70010=0.
compute n70020=0.
compute n70030=0.
compute n70070=0.
compute n70080=0.
compute n70083=0.
compute n70091=0.
compute n70100=0.
compute n70101=0.
compute n70111=0.
compute n70120=0.
compute n70121=0.
compute n70130=0.
compute n70140=0.
compute n70150=0.
compute n70151=0.
compute n70170=0.
compute n70180=0.
compute n70210=0.
compute n70220=0.
compute n70230=0.
compute n70240=0.
compute n70241=0.
compute n70250=0.
compute n70260=0.
compute n70280=0.
compute n76010=0.
compute n76011=0.
compute n76012=0.
compute n76030=0.
compute n76040=0.
compute n76041=0.
compute n76042=0.
compute n76050=0.
compute n76060=0.


if (tnb=70010) n70010=1.0.
if (tnb=70020) n70020=1.0.
if (tnb=70030) n70030=1.0.
if (tnb=70070) n70070=1.0.
if (tnb=70080) n70080=1.0.
if (tnb=70083) n70083=1.0.
if (tnb=70091) n70091=1.0.
if (tnb=70100) n70100=1.0.
if (tnb=70101) n70101=1.0.
if (tnb=70111) n70111=1.0.
if (tnb=70120) n70120=1.0.
if (tnb=70121) n70121=1.0.
if (tnb=70130) n70130=1.0.
if (tnb=70140) n70140=1.0.
if (tnb=70150) n70150=1.0.
if (tnb=70151) n70151=1.0.
if (tnb=70170) n70170=1.0.
if (tnb=70180) n70180=1.0.
if (tnb=70210) n70210=1.0.
if (tnb=70220) n70220=1.0.
if (tnb=70230) n70230=1.0.
if (tnb=70240) n70240=1.0.
if (tnb=70241) n70241=1.0.
if (tnb=70250) n70250=1.0.
if (tnb=70260) n70260=1.0.
if (tnb=70280) n70280=1.0.
if (tnb=76010) n76010=1.0.
if (tnb=76011) n76011=1.0.
if (tnb=76012) n76012=1.0.
if (tnb=76030) n76030=1.0.
if (tnb=76040) n76040=1.0.
if (tnb=76041) n76041=1.0.
if (tnb=76042) n76042=1.0.
if (tnb=76050) n76050=1.0.
if (tnb=76060) n76060=1.0.
               	
Compute lsf=sqftl.
Compute nlsf=n*lsf.
Compute nbsf=n*bsf.
Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*srbsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=npremrf*bsf.


Compute firp2=0.
If firepl=1 firp2=1.
If firepl>=2 firp2=2.
compute nfirpl=0.
compute nfirpl=n*firp2.


compute nogarage=0.
if gar=7 nogarage=1.
Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=0.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.
compute nfulbath=n*fullbath.
compute nhafbath=n*halfbath.
compute nbathsum=n*bathsum.

If num=6 bnum=0.
If num=0 bnum=0.
If num=1 bnum=2.
If num=2 bnum=3.
If num=3 bnum=4.
If num=4 bnum=5.
If num=5 bnum=6.
compute nbnum=n*bnum.
compute bnumb=bnum*bsf.


compute b=1.
*select if (year1=2008).
*select if puresale=1.
*Table observation = b
                amount1
     /table = tnb by 
              amount1 
 		             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').


reg des=defaults cov
     /var=amount1 nsrage nsrbsf nbaspart nnobase nbasfull nbsfair nsrlsf nrepabsf
	garage1 garage2 biggar nfirpl nluxbsf nbsf56 nbsf778 nbsf34 nbsf1095 nbathsum bnumb masbsf frmsbsf
	frabsf stubsf nrepbesf nprembsf sb70010 sb70020 sb70030 sb70070 sb70080 sb70083 sb70091
	sb70100 sb70101 sb70111 sb70120 sb70121 sb70130 sb70140 sb70150 sb70151 sb70170 sb70180 sb70210
	sb70220 sb70230 sb70240 sb70241 sb70250 sb70260 sb70280 sb76011 sb76012
	sb76030 sb76041 sb76042 sb76050 sb76060 winter0405cl234 winter0506cl234
	winter0607cl234 winter0708cl234 summer04cl234 summer05cl234 summer06cl234
	summer07cl234 jantmar04cl234 octtdec08cl234 winter0405cl56 winter0506cl56 
	winter0607cl56 winter0708cl56 summer04cl56 	summer05cl56 summer06cl56 summer07cl56
	jantmar04cl56 octtdec08cl56 winter0405cl778 winter0506cl778 winter0607cl778 
	winter0708cl778 summer04cl778 summer05cl778 summer06cl778 summer07cl778 jantmar04cl778
	octtdec08cl778 winter0405cl89 winter0506cl89 winter0607cl89 winter0708cl89 summer04cl89
	summer05cl89 summer06cl89 summer07cl89 jantmar04cl89 octtdec08cl89  	
	winter0405cl1112 winter0506cl1112 winter0607cl1112 winter0708cl1112 summer04cl1112
	summer05cl1112 summer06cl1112 summer07cl1112 jantmar04cl1112 octtdec08cl1112  	
     winter0405clsplt winter0506clsplt winter0607clsplt winter0708clsplt summer04clsplt
	summer05clsplt summer06clsplt summer07clsplt jantmar04clsplt octtdec08clsplt  	
     winter0405cl1095 winter0506cl1095 winter0607cl1095 winter0708cl1095 summer04cl1095
	summer05cl1095 summer06cl1095 summer07cl1095 jantmar04cl1095 octtdec08cl1095 
	lowzonesouth midzonehydepark midzonesouth highzonehydepark lowzonehydepark
	srfxlowblockhydepark srfxlowblocksouth srfxmidblockhydepark 
	srfxmidblocksouth srfxhighblockhydepark srfxhighblocksouth   
	/dep=amount1 
	/method=stepwise   
	/method=enter winter0405cl234 winter0506cl234 winter0607cl234 winter0708cl234 summer04cl234
	summer05cl234 summer06cl234 summer07cl234 jantmar04cl234 octtdec08cl234
     winter0405cl56 winter0506cl56 winter0607cl56 winter0708cl56 summer04cl56
	summer05cl56 summer06cl56 summer07cl56 jantmar04cl56 octtdec08cl56  	
     winter0405cl778 winter0506cl778 winter0607cl778 winter0708cl778 summer04cl778
	summer05cl778 summer06cl778 summer07cl778 jantmar04cl778 octtdec08cl778  	
     winter0405cl89 winter0506cl89 winter0607cl89 winter0708cl89 summer04cl89
	summer05cl89 summer06cl89 summer07cl89 jantmar04cl89 octtdec08cl89  	
	winter0405cl1112 winter0506cl1112 winter0607cl1112 winter0708cl1112 summer04cl1112
	summer05cl1112 summer06cl1112 summer07cl1112 jantmar04cl1112 octtdec08cl1112  	
     winter0405clsplt winter0506clsplt winter0607clsplt winter0708clsplt summer04clsplt
	summer05clsplt summer06clsplt summer07clsplt jantmar04clsplt octtdec08clsplt  	
     winter0405cl1095 winter0506cl1095 winter0607cl1095 winter0708cl1095 summer04cl1095
	summer05cl1095 summer06cl1095 summer07cl1095 jantmar04cl1095 octtdec08cl1095
	/method=enter sb70100 sb70260 
	/method=enter lowzonesouth midzonehydepark midzonesouth highzonehydepark lowzonehydepark
	srfxlowblockhydepark srfxlowblocksouth srfxmidblockhydepark 
	srfxmidblocksouth srfxhighblockhydepark srfxhighblocksouth
	/method=enter frmsbsf 
		/save pred (pred) resid (resid).
           sort cases by tnb pin.	
           value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
          /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
          /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
          /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
          /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11 '2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount1 with RESID.
*compute badsal=0.
*if perdif>75 badsal=1.
*if perdif<-75 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Hyde Park'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
