# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

# Top comments ----

# This script builds the data required to value 200 class properties in a given township. The script assumes the user
# has a pre-defined directory structure on their local terminal, and is operating
# from within CCAO's network. Modification may be required for others.
#
# Version 0.1
# Written 01/10/2018
# Updated 01/23/2018 to account for MULTIs and other subgroups
# Updated XX/XX/XXXX

load(destfile)

# connect to retrieve data
CCAODATA <- dbConnect(odbc()
                      , driver   = "SQL Server"
                      , server   = odbc.credentials("server")
                      , database = odbc.credentials("database")
                      , uid      = odbc.credentials("uid")
                      , pwd      = odbc.credentials("pwd"))

# SQL Pulls----
n_ <-
    dbGetQuery(CCAODATA,
  paste0("
SELECT modeling_group
, CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) AS TOWNSHIP
, COUNT(HD_PIN) as PINs, COUNT(DISTINCT HD_PIN) as D_PINs
,  COUNT(HD_PIN)-COUNT(DISTINCT HD_PIN) as DUPLICATES
,  CASE WHEN MULTI_IND IS NULL THEN 0 ELSE MULTI_IND END AS MULTI_IND
FROM
(SELECT *
, CASE WHEN HD_CLASS IN (200, 201, 241, 299) THEN 'NCHARS'
    WHEN HD_CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295) THEN 'SF'
    WHEN HD_CLASS IN (211, 212) THEN 'MF'
    ELSE NULL END AS modeling_group
FROM HEAD) AS H
LEFT JOIN
CCAOSFCHARS AS C
ON H.HD_PIN=C.PIN AND H.TAX_YEAR=C.TAX_YEAR
WHERE (1=1)
AND H.TAX_YEAR=2018
AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN (", paste(target_township, collapse = ', '), ")
AND HD_CLASS IN (211,212, 200, 201, 241, 299, 202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295)
GROUP BY modeling_group, CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)), MULTI_IND
ORDER BY modeling_group, TOWNSHIP
"))

assmntdata <- dbGetQuery(CCAODATA, paste0("
SELECT * FROM VW_ASSMNTDATA WHERE TOWN_CODE IN (", paste(target_township, collapse = ', ') ,")
"))

# if current tax year is same also a  reassessment year for target_township, years prior should be 4
# if current tax year is not a reassessment year for target_township, make sure to keep in consideration for which years
# 288s are valid when defining years_prior, i.e. a 288 can last up to 6 years depending on when it was filed
addchars <- dbGetQuery(CCAODATA, paste0("
SELECT QU_PIN as PIN, TAX_YEAR, QU_MLT_CD, QU_UPLOAD_DATE
  , QU_TYPE_OF_RES, QU_USE, QU_NUM_APTS, QU_EXTERIOR_WALL, QU_ROOF, QU_ROOMS, QU_BEDS, QU_BASEMENT_TYPE
  , QU_BASEMENT_FINISH, QU_HEAT, QU_AIR, QU_FIRE_PLACE, QU_ATTIC_TYPE, QU_ATTIC_FINISH, QU_FULL_BATH, QU_HALF_BATH
  , QU_TYPE_PLAN, QU_TYPE_DESIGN, QU_CONSTRUCT_QUALITY, QU_RENOVATION, QU_SITE_DESIRE, QU_GARAGE_SIZE
  , QU_GARAGE_CONST, QU_GARAGE_ATTACHED, QU_GARAGE_AREA, QU_PORCH, QU_SQFT_BLD, QU_STATE_OF_REPAIR
  , QU_NO__COM_UNIT
FROM ADDCHARS
WHERE QU_HOME_IMPROVEMENT = 1 AND TAX_YEAR <= ", tax_year - 4, "
AND QU_TOWN IN (", paste(target_township, collapse = ', '), ")"
))

# disconnect after pulls
dbDisconnect(CCAODATA)

freq_table <- join(n_,
                   setNames(aggregate(assmntdata$cons, by=list(Category=assmntdata$modeling_group, assmntdata$TOWN_CODE, assmntdata$MULTI_IND), FUN=sum)
                            , c("modeling_group", "TOWNSHIP", "MULTI_IND", "ASSMNT_PINS")))

# Check for record count
integ_check_f(sum(freq_table[3], na.rm = TRUE)!= sum(freq_table[7], na.rm = TRUE),
              paste0("Correct number of records in assessment data pull = ", sum(freq_table[3])),
              paste0("Bad: There are ", sum(freq_table[3], na.rm = TRUE)- sum(freq_table[7], na.rm = TRUE), " records missing from the assessment data"),
              "GOOD: Correct number of rows in assessment data data",
              "0")
# Check for duplciates
integ_check_f(length(unique(subset(assmntdata, MULTI_IND!=1)$PIN))!=nrow(subset(assmntdata, MULTI_IND!=1)),
              paste0("ASSMNT data is unique by PIN afer server pull"),
              paste0("Bad: ", nrow(subset(assmntdata, MULTI_IND!=1))-length(unique(subset(assmntdata, MULTI_IND!=1)$PIN)), " Duplicate PINs in the ASSMNT data"),
              "GOOD: No duplicate in the ASSMNT data",
              "0")
# Check for erroneous PER_ASS values
integ_check_f(nrow(subset(assmntdata, CLASS == 299 & is.na(PER_ASS)))>0,
              paste0("PER_ASS values are sensical"),
              paste0("Bad: ", nrow(subset(assmntdata, CLASS == 299 & is.na(PER_ASS))), " NA values in PER_ASS"),
              "GOOD: PER_ASS values are all finite",
              "0")
integ_check_f(nrow(subset(assmntdata, !is.na(PER_ASS) & (PER_ASS<=0 | PER_ASS>1)!=0)),
              paste0("PER_ASS values are sensical"),
              paste0("Bad: ", nrow(subset(assmntdata, !is.na(PER_ASS) & (PER_ASS<=0 | PER_ASS>1)!=0)), " PER_ASS values outside [0,1)"),
              "GOOD: PER_ASS values are greater than 0 and less than or equal to 1 where finite",
              "0")

# add 288 data
assmntdata<-addchars_func(assmntdata, "assmntdata")

# Check for record count
integ_check_f(sum(freq_table[3])!= sum(freq_table[7], na.rm = TRUE),
              paste0("Correct number of records in assessment data pull = ", sum(freq_table[3])),
              paste0("Bad: There are ", sum(freq_table[3])- sum(freq_table[7]), " records missing from the assessment data"),
              "GOOD: Correct number of rows in assessment data data",
              "0")
# Check for duplciates
integ_check_f(length(unique(subset(assmntdata, MULTI_IND!=1)$PIN))!=nrow(subset(assmntdata, MULTI_IND!=1)),
              paste0("ASSMNT data is unique by PIN afer server pull"),
              paste0("Bad: ", nrow(subset(assmntdata, MULTI_IND!=1))-length(unique(subset(assmntdata, MULTI_IND!=1)$PIN)), " Duplicate PINs in the ASSMNT data"),
              "GOOD: No duplicate in the ASSMNT data",
              "0")
# Check for erroneous PER_ASS values
integ_check_f(nrow(subset(assmntdata, is.na(PER_ASS)))>0,
              paste0("PER_ASS values are sensical"),
              paste0("Bad: ", nrow(subset(assmntdata, is.na(PER_ASS))), " NA values in PER_ASS"),
              "GOOD: PER_ASS values are all finite",
              "0")
integ_check_f(nrow(subset(assmntdata, !is.na(PER_ASS) & (PER_ASS<=0 | PER_ASS>1)!=0)),
              paste0("PER_ASS values are sensical"),
              paste0("Bad: ", nrow(subset(assmntdata, !is.na(PER_ASS) & (PER_ASS<=0 | PER_ASS>1)!=0)), " PER_ASS values outside [0,1)"),
              "GOOD: PER_ASS values are greater than 0 and less than or equal to 1 where finite",
              "0")

# update 299 pins with 'GR' CDUs
grs <- read.xlsx("ccao_dictionary_draft.xlsx", sheetName = "Parking Spaces", stringsAsFactors=FALSE)
for (i in 1:ncol(grs)){
  grs[,i][nchar(grs[,i]) == 13 & !is.na(grs[,i])]<-paste0("0", grs[,i][nchar(grs[,i]) == 13 & !is.na(grs[,i])])
}
grs<-grs[,which(colnames(grs) %in% paste0('X', target_township))]
if (ncol(data.frame(grs)) > 1){
  grs <- stack(grs)[stack(grs)[,1] != "",1]
}
assmntdata$CDU[assmntdata$PIN %in% grs[!is.na(grs)]] <- 'GR'

# update modeling groups for pins with 'GR' CDUs
assmntdata$modeling_group[assmntdata$PIN %in% grs] <- 'FIXED'

# Set dates at specified date ----
assmntdata$sale_date <- estimation_date
assmntdata$sale_date <- as.Date(assmntdata$sale_date, "%Y%m%d")
assmntdata$sale_year <- lubridate::year(assmntdata$sale_date)
assmntdata$sale_month <-(assmntdata$sale_year-1997)*12+lubridate::month(assmntdata$sale_date)
assmntdata$sale_quarter <- (assmntdata$sale_year-1997)*4+lubridate::quarter(assmntdata$sale_date)
assmntdata$sale_halfyear <- (assmntdata$sale_year-1997)*2+car::recode(as.numeric(as.character(substr(quarters(assmntdata$sale_date),2,2))), "1:2 = 1; 3:4 = 2")
assmntdata$sale_quarterofyear <- lubridate::quarter(assmntdata$sale_date)
assmntdata$sale_monthofyear <-as.numeric(format(assmntdata$sale_date, "%m"))
assmntdata$sale_halfofyear <- ifelse(as.numeric(as.character(substr(quarters(assmntdata$sale_date),2,2)))<=2,1,2)
assmntdata <- e.recode$fac(assmntdata, c("sale_halfofyear", "sale_monthofyear", "sale_quarterofyear"))

# Numeric variables ----
# SAME THING HERE WE WANT TO LOOP OVER A LIST
assmntdata <- e.recode$num(assmntdata, c("centroid_x", "centroid_y", "BLDG_SF", "FBATH", "HBATH", "AGE"))
assmntdata$AGE_SQRD<-as.numeric(assmntdata$AGE)^2
assmntdata$AGE_DECADE<-round(as.numeric(assmntdata$AGE)/10,1)
assmntdata$AGE_DECADE_SQRD<-round(as.numeric(assmntdata$AGE)/10,1)^2
assmntdata$most_recent_sale_price <- as.numeric(assmntdata$most_recent_sale_price)
assmntdata$ROOMS[assmntdata$ROOMS==0]<-NA
assmntdata$centroid_x_demeaned<-(assmntdata$centroid_x-mean(assmntdata$centroid_x))
assmntdata$centroid_y_demeaned<-(assmntdata$centroid_y-mean(assmntdata$centroid_y))
assmntdata$HD_SF_SQRD<-as.numeric(assmntdata$HD_SF)^2
assmntdata$BLDG_SF_SQRD<-as.numeric(assmntdata$BLDG_SF)^2
assmntdata$withinmr100 <- as.numeric(assmntdata$withinmr100)
assmntdata$withinmr101300 <- as.numeric(assmntdata$withinmr101300)
assmntdata$ppsf_bldg <- assmntdata$most_recent_sale_price / assmntdata$BLDG_SF
assmntdata$ppsf_lot <- assmntdata$most_recent_sale_price / assmntdata$HD_SF

# Check integrity
integ_check_f(anyNA(assmntdata$centroid_x)==TRUE,
              paste0("All assessment PINs have xy coordinates"),
              paste0("Bad: No, some PINs are missing xy coordinates"),
              paste0("Good: All PINs have xy coordinates"),
              "0")

# Location factor ----
# LOCF_MODEL expects 'sale_price' as the dep var
colnames(assmntdata)[colnames(assmntdata)=="most_recent_sale_price"] <- "sale_price"
assmntdata$LOC_PRED <- as.numeric(predict(LOCF_MODEL,newdata=assmntdata))
colnames(assmntdata)[colnames(assmntdata)=="sale_price"] <- "most_recent_sale_price"
assmntdata$LOCF1 <- assmntdata$LOC_PRED/mean(assmntdata$LOC_PRED)

# Check for NAs
integ_check_f(anyNA(assmntdata$LOCF1)==TRUE,
              paste0("All assessment PINs have LOCF values"),
              paste0("Bad: No, some PINs are missing LOCF values"),
              paste0("Good: All PINs have LOCF values"),
              "0")
# Factor variables ----
# factors <- c("TOWN_CODE") HELP GET THIS WORKING
# for(f in 1:length(factors)){
#   eval(modeldata$factors[f])<-factor(eval(modeldata$factors[f]))
# }

assmntdata$MULTI_FAMILY_IND<- factor(assmntdata$MULTI_FAMILY_IND)
#assmntdata$census_tract<-factor(assmntdata$census_tract)

assmntdata$PIN10[assmntdata$modeling_group == 'NCHARS']<-substr(assmntdata$PIN[assmntdata$modeling_group == 'NCHARS'], 1, 10)
assmntdata$PIN10F<-as.factor(assmntdata$PIN10)
assmntdata$PIN10F_OG<-assmntdata$PIN10F
assmntdata$FRPL<-factor(ifelse(assmntdata$FRPL>=2, 2, assmntdata$FRPL))
assmntdata$condo_strata_10[assmntdata$CONDO_CLASS_FACTOR == 200] <- 1
assmntdata$condo_strata_10[assmntdata$assmnting_group == 'FIXED'] <- NA
assmntdata$condo_strata_10<-factor(assmntdata$condo_strata_10, levels = as.character(seq(1:10)), ordered = TRUE)
assmntdata$condo_strata_100[assmntdata$CONDO_CLASS_FACTOR == 200] <- 1
assmntdata$condo_strata_100[assmntdata$assmnting_group == 'FIXED'] <- NA
assmntdata$condo_strata_100<-factor(assmntdata$condo_strata_100, levels = as.character(seq(1:100)), ordered = FALSE)
assmntdata$FRPL <- as.numeric(assmntdata$FRPL)
assmntdata$FRPL<-factor(ifelse(assmntdata$FRPL>=2, 2, assmntdata$FRPL))
assmntdata$EXT_WALL<-as.numeric(factor(car::recode(assmntdata$EXT_WALL, "0=NA")))
assmntdata$EXT_WALL<-as.factor(assmntdata$EXT_WALL)
assmntdata$GARAGE_IND <- factor(ifelse(assmntdata$GAR1_SIZE==7,0,1))
assmntdata$CLASS<-as.factor(assmntdata$CLASS)
assmntdata$census_tract<-factor(assmntdata$census_tract)
assmntdata$CONDO_CLASS_FACTOR<-as.factor(assmntdata$CONDO_CLASS_FACTOR)
assmntdata$BSMT_FIN<-factor(ifelse(assmntdata$BSMT_FIN==0, NA, assmntdata$BSMT_FIN))
assmntdata$BSMT[assmntdata$BSMT==0]<-NA
assmntdata$BSMT<-factor(assmntdata$BSMT)
assmntdata$AIR[assmntdata$AIR==0]<-NA
assmntdata$AIR[assmntdata$AIR==2]<-0
assmntdata$AIR <- factor(assmntdata$AIR)
assmntdata$ROOF_CNST<-factor(ifelse(assmntdata$ROOF_CNST==0, NA, assmntdata$ROOF_CNST))
assmntdata$HEAT<-factor(car::recode(assmntdata$HEAT, "0=NA; 3:4=2"))
assmntdata$BEDS[assmntdata$BEDS==0]<-NA
assmntdata$OHEAT<-factor(assmntdata$OHEAT)
assmntdata$ATTIC_TYPE<-factor(ifelse(assmntdata$ATTIC_TYPE==0, NA, assmntdata$ATTIC_TYPE))
assmntdata$CNST_QLTY<-factor(ifelse(assmntdata$CNST_QLTY==0, NA, assmntdata$CNST_QLTY))
assmntdata$SITE<-factor(ifelse(assmntdata$SITE==0, NA, assmntdata$SITE))
assmntdata$GAR1_SIZE <- car::recode(assmntdata$GAR1_SIZE, "7=0; 8=7")
assmntdata <- e.recode$fac(assmntdata, c("GAR1_CNST", "GAR1_ATT", "GAR1_AREA", "PORCH"))
assmntdata$REPAIR_CND<-factor(ifelse(assmntdata$REPAIR_CND==0, NA, assmntdata$REPAIR_CND))
# we are explicity assuming that NULL PORCH values indicate that a improvement does not possess a porch
assmntdata$PORCH<-as.numeric(assmntdata$PORCH)
assmntdata$PORCH<-as.factor(ifelse(is.na(assmntdata$PORCH)==TRUE, 3, assmntdata$PORCH))
assmntdata$ohare_noise<-as.factor(assmntdata$ohare_noise)
assmntdata$large_home <- ifelse(assmntdata$CLASS %in% c(208, 209), 1, 0)
assmntdata$floodplain <- as.numeric(assmntdata$floodplain)

# Adjust PER_ASS for buildings with non-residential units, part 1
per_ass <- subset(assmntdata, CLASS == 299) %>%
  dplyr::group_by(substr(subset(assmntdata, CLASS == 299)$PIN,1,10)) %>%
  summarise(PER_ASS_BUILDING = sum(PER_ASS)
          , total_units = mean(total_units)
          , res_units = length(PIN))
assmntdata$temp <- substr(assmntdata$PIN,1,10)
colnames(per_ass)[1] <- "temp"

# Some condos have errorneous decks in the 'DETAIL' SQL table
assmntdata$PER_ASS[assmntdata$PIN == '10134190421001'] <- 0.4
assmntdata$PER_ASS[assmntdata$PIN == '10134190421002'] <- 0.6
per_ass$PER_ASS_BUILDING[per_ass$temp == '1013419042'] <- 1

# Adjust PER_ASS for buildings with non-residential units, part 2
assmntdata <- merge(assmntdata, dplyr::select(per_ass, c("temp", "PER_ASS_BUILDING")), by = "temp", all.x = TRUE)
assmntdata$PER_ASS <- assmntdata$PER_ASS / assmntdata$PER_ASS_BUILDING
assmntdata$temp <- NULL
colnames(per_ass)[1] <- "PIN10"

# Some condos have errorneous decks in the 'DETAIL' SQL table
assmntdata$PER_ASS[assmntdata$PIN == '10134190421001'] <- 0.4
assmntdata$PER_ASS[assmntdata$PIN == '10134190421002'] <- 0.6
per_ass$PER_ASS_BUILDING[per_ass$PIN10 == '1013419042'] <- 1

# export condos that need to be manually checked
per_ass$diff <- per_ass$total_units - per_ass$res_units
per_ass$PIN10[per_ass$PER_ASS_BUILDING < .999] <-
  paste(substr(per_ass$PIN10[per_ass$PER_ASS_BUILDING < .999], 1, 2)
      , substr(per_ass$PIN10[per_ass$PER_ASS_BUILDING < .999], 3, 4)
      , substr(per_ass$PIN10[per_ass$PER_ASS_BUILDING < .999], 5, 7)
      , substr(per_ass$PIN10[per_ass$PER_ASS_BUILDING < .999], 8, 10)
      , sep = "-")
write.xlsx(data.frame(dplyr::select(subset(per_ass, PER_ASS_BUILDING < .999)
                                    , c("PIN10", "PER_ASS_BUILDING", "diff")))
           , file = paste0(dirs$results_desk_review, "condos_", paste(scope_target), ".xlsx"), row.names=FALSE, showNA=FALSE)
rm(per_ass)

# CREATED TEMP HERE

# Initial filter ----
# (user defined exclusions due to data issues, observations with filter_1 value of "1" will be used)
# filter_1 is our "pure market" indicator
assmntdata$puremarket <- 0
assmntdata$puremarket<-ifelse(
  (assmntdata$modeling_group!="NCHARS"
   & assmntdata$most_recent_sale_price>100 & assmntdata$most_recent_sale_price<10000000
  & is.finite(assmntdata$most_recent_sale_price)
  & assmntdata$BLDG_SF>100 & is.finite(assmntdata$BLDG_SF)) |
    (assmntdata$modeling_group=="NCHARS" & assmntdata$most_recent_sale_price>100 & assmntdata$most_recent_sale_price<1000000
  & is.finite(assmntdata$most_recent_sale_price))
  ,1,0)
assmntdata$filter_1 <- assmntdata$puremarket

# Geographic recodes ----
assmntdata <- e.recode$num(assmntdata, c("TOWN_CODE", "NBHD", "census_tract"))

assmntdata$NBHD_mapping <- assmntdata$NBHD

# Variables for legacy model
assmntdata <- legacy_recodes(assmntdata)

# generate combined neighborhood/towncode variable specific to modeling groups to avoid having to use interaction terms
assmntdata$town_nbhd <- paste0(assmntdata$TOWN_CODE, assmntdata$NBHD)

# aggregate foreclosure data
foreclosures_quarter_nbhd <- foreclosures %>% dplyr::group_by(town_nbhd, QUARTER) %>% tally(name = "foreclosures_quarter_nbhd")
foreclosures_month_nbhd <- foreclosures %>% dplyr::group_by(town_nbhd, MONTH) %>% tally(name = "foreclosures_month_nbhd")
foreclosures_quarter_block <- foreclosures %>% dplyr::group_by(BLOCK, QUARTER) %>% tally(name = "foreclosures_quarter_block")

assmntdata$block <- substr(assmntdata$PIN, 1, 7)

# change assmntdata time trend variables for join
assmntdata$sale_quarter <- assmntdata$sale_quarter - 1
assmntdata$sale_month <- assmntdata$sale_month - 1

# join foreclosure data
assmntdata <- dplyr::left_join(assmntdata, foreclosures_quarter_nbhd,  by = c("sale_quarter" = "QUARTER", "town_nbhd"))
assmntdata <- dplyr::left_join(assmntdata, foreclosures_month_nbhd,    by = c("sale_month" = "MONTH", "town_nbhd"))
assmntdata <- dplyr::left_join(assmntdata, foreclosures_quarter_block, by = c("sale_quarter" = "QUARTER", "block" = "BLOCK"))

# change assmntdata time trend variables back
assmntdata$sale_quarter <- assmntdata$sale_quarter + 1
assmntdata$sale_month <- assmntdata$sale_month + 1

assmntdata$foreclosures_quarter_nbhd[is.na(assmntdata$foreclosures_quarter_nbhd)] <- 0
assmntdata$foreclosures_month_nbhd[is.na(assmntdata$foreclosures_month_nbhd)] <- 0
assmntdata$foreclosures_quarter_block[is.na(assmntdata$foreclosures_quarter_block)] <- 0

# needed as factor moving on
assmntdata$town_nbhd <- as.factor(assmntdata$town_nbhd)
assmntdata$modeling_group <- as.factor(assmntdata$modeling_group)

for (i in levels(assmntdata$modeling_group)){
  eval(parse(text = paste0("assmntdata$town_nbhd_", i, " <- assmntdata$town_nbhd")))
  eval(parse(text = paste0("assmntdata$town_nbhd_", i, "[assmntdata$modeling_group!='", i, "']<-NA")))
}

# reassign neighborhoods that lacked sales in modeldata construction
assmntdata <- geo_recodes(assmntdata)

for (i in c('SF', 'MF', 'NCHARS')){
  eval(parse(text = paste0("assmntdata$town_nbhd_", i, "<-droplevels(assmntdata$town_nbhd_", i, ")")))
}

# Want as factor once geographic recodes are completed
assmntdata <- e.recode$fac(assmntdata, c("TOWN_CODE", "NBHD", "census_tract"))

# print potentially problematic neighborhoods
for(i in levels(factor(modeldata$modeling_group, exclude = 'FIXED'))){
  if(length(eval(parse(text = paste0("levels(assmntdata$town_nbhd_", i, ")[levels(assmntdata$town_nbhd_", i, ") %ni% levels(modeldata$town_nbhd_", i, ")]")))) > 0){
    print(paste0("Neighborhoods in modeling group ", i, " with no sales:"))
    print(eval(parse(text = paste0("levels(assmntdata$town_nbhd_", i, ")[levels(assmntdata$town_nbhd_", i, ") %ni% levels(modeldata$town_nbhd_", i, ")]"))))
  }
  if(length(eval(parse(text = paste0("as.character(data.frame(table(modeldata$town_nbhd_", i, "))$Var1[data.frame(table(modeldata$town_nbhd_", i, "))$Freq == 1])")))) > 0){
    print(paste0("Neighborhoods in modeling group ", i, " with one sale:"))
    print(eval(parse(text = paste0("as.character(data.frame(table(modeldata$town_nbhd_", i, "))$Var1[data.frame(table(modeldata$town_nbhd_", i, "))$Freq == 1])"))))
    print(noquote(""))
  }
}

#Predict price tercile using ordered logic model
assmntdata$price_tercile <- NA
for(m in c('SF', 'MF', 'NCHARS')){
  model <- paste("TERC_MOD_", m, sep = "")
  err <- try(predict(object=eval(parse(text=model)), newdata=subset(assmntdata, modeling_group==m), na.action = na.exclude))
  if(class(err)!="try-error"){
    assmntdata$price_tercile[assmntdata$modeling_group==m] <-
      predict(object=eval(parse(text=model)), newdata=subset(assmntdata, modeling_group==m), na.action = na.exclude)
    check<- paste0("Price Tercile for ", paste(m), " in Assmntdata")
    msg<- paste0("Good: Price Tercile assigned")
    integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
    print(check); print(msg); rm(check, msg)
  }else{
    check<- paste0("Price Tercile for ", paste(m), " in Assmntdata")
    msg<- paste0("Bad: Price Tercile not assigned")
    integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
    print(check); print(msg); rm(check, msg)
  }
}
assmntdata$price_tercile<-as.factor(assmntdata$price_tercile)
assmntdata$price_tercile<-ordered(assmntdata$price_tercile, c(1, 2, 3))

#tercile integrity checks
integ_check_f(anyNA(assmntdata$price_tercile)==TRUE,
              paste0("All assessment PINs have price tercile value?"),
              paste0("Bad: No, some PINs don't have price tercile value"),
              paste0("Good: All PINs have price tercile value"),
              "0")

a_tercile_integ <- assmntdata[,-8] %>%
  dplyr::group_by(modeling_group, price_tercile) %>%
  dplyr::summarise(count = n(), median.sale.price = median(most_recent_sale_price, na.rm=TRUE))
a_tercile_integ <- as.data.frame(a_tercile_integ)
mfsum <- a_tercile_integ %>% filter(modeling_group=="MF")
sfsum <- a_tercile_integ %>% filter(modeling_group=="SF")
#ncsum <- a_tercile_integ %>% filter(modeling_group=="NCHARS")

integ_check_f(is.unsorted(mfsum$median.sale.price[!is.na(mfsum$median.sale.price)]),
              paste0("Do MF price tercile median sale values increase in each tercile?"),
              paste0("Bad: median price does not increase"),
              paste0("Good: median price increases"),
              "0")
integ_check_f(is.unsorted(sfsum$median.sale.price[!is.na(sfsum$median.sale.price)]),
              paste0("Do SF price tercile median sale values increase in each tercile?"),
              paste0("Bad: median price does not increase"),
              paste0("Good: median price increases"),
              "0")

#if(is.unsorted(ncsum$median.sale.price)){
#  check<- paste0("Do NCHARS price tercile median sale values increase in each tercile?")
#  msg<- paste0("Bad: median price does not increase")
#  integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#  print(msg); rm(check, msg)
#}else{
#  check<- paste0("Do NCHARS price tercile median sale values increase in each tercile?")
#  msg<- paste0("Good: median price increases")
#  integrity_checks <- rbind.fill(integrity_checks, data.frame(check=check, outcome=msg))
#  print(msg); rm(check, msg)
#}

remove(mfsum, sfsum) #add ncsum to this list if using NCHARS

# adjust price terciles for consistency within class and neighborhood to make sure there are no major discontinuities
# between similar properties on the margin of price tercile
for (x in levels(assmntdata$CLASS)){
  for (y in levels(assmntdata$town_nbhd_SF)){
    assmntdata$price_tercile[assmntdata$CLASS == x & assmntdata$town_nbhd_SF == y & assmntdata$modeling_group == 'SF'] <- round(mean(as.numeric(assmntdata$price_tercile[assmntdata$CLASS == x & assmntdata$town_nbhd_SF == y & assmntdata$modeling_group == 'SF']), na.rm = TRUE), 0)
  }
}

for (x in levels(assmntdata$CLASS)){
  for (y in levels(assmntdata$town_nbhd_MF)){
    assmntdata$price_tercile[assmntdata$CLASS == x & assmntdata$town_nbhd_MF == y & assmntdata$modeling_group == 'MF'] <- round(mean(as.numeric(assmntdata$price_tercile[assmntdata$CLASS == x & assmntdata$town_nbhd_MF == y & assmntdata$modeling_group == 'MF']), na.rm = TRUE), 0)
  }
}

# legacy weights Dave uses
for(t in levels(assmntdata$TOWN_CODE)){
  for(n in levels(assmntdata$NBHD)){
    assmntdata$legacy_weight[assmntdata$TOWN_CODE==t & assmntdata$NBHD==n] <-
      median(subset(assmntdata, !is.na(most_recent_sale_price) & TOWN_CODE==t & NBHD==n)$most_recent_sale_price)/100000
  }
}

# problematic pins excluded, usually due to missing data
assmntdata <- subset(assmntdata, !(assmntdata$PIN %in% read.xlsx("ccao_dictionary_draft.xlsx", sheetName = "pin_exclusions", stringsAsFactors=FALSE)[,1]))

summary(as.factor(subset(assmntdata, filter_1>=1)$modeling_group))
file_info <- update_file_info('0_build_200class_assmtdata')
# Save ----
save(list = scope_list[sapply(scope_list, exists)==TRUE], file=destfile)
