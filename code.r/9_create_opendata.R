# This script loops over each township in current assesment triennial and create first-pass value open data.

library(plyr)
library(dplyr)

#Define the townships in current assesment triennial
curr_towns <- c(26, 17, 23, 16, 22, 25, 10, 20, 38, 29, 35, 24, 18)

scopes <- c()
for (num in curr_towns){
  scope_file <- paste0("scope_", num, "_2019.Rda")
  scopes <- append(scopes, scope_file, after=length(scopes))
}

# Define the column names which we need and their appropriate names for opendata
full_cols <- c("PIN","ADDR", "most_recent_sale_date", 
               "DOC_NO", "most_recent_sale_price", 
               "puremarket", "PRI_EST_LAND", 
               "PRI_EST_BLDG", "fitted_value_1",
               "fitted_value_2", "land_value",
               "improvement_value", "fitted_value_3",
               "fitted_value_4", "fitted_value_5",
               "fitted_value_6", "fitted_value_7")
target_cols <- c("PIN", "Property Address","Most Recent Date Sale", 
                 "Deed No.", "Most Recent Sale Price","Pure Market Indicator",
                 "Estimate (Land)", "Estimate (Building)", "First Pass Value 1",
                 "First Pass Value 2", "Land Value", "Improvement Value",
                 "First Pass Value 3", "First Pass Value 4", "First Pass Value 5",
                 "First Pass Value 6", "First Pass Value 7")

# Update the opendata
agg_df <- NULL
file_names <- list.files("O:/CCAODATA/results/final_values/")
for (scope in scopes){
  curnum <- substring(scope, 7, 8)
  if (!(scope %in% file_names)){
    print(paste("Scope file does not exist for township code", curnum))}
  else {
    curfile <- paste("O:/CCAODATA/results/final_values/", scope, sep="")
    e <- new.env()
    load(curfile, e)
    if (!is.null(e$"file_info")){
      print(curnum)
      file_info <- print_file_info(curnum)
      actions <- c()
      for (action in file_info["action"]){
        actions <- append(actions, as.character(action), after=length(actions))
      }
      if (!("8_complete_firstpass" %in% actions)){
        print(paste("Township code", curnum, "is not complete for first-pass value"))
      }else{
        valuation_df <- e$valuationdata
        valuation_df <- valuation_df %>%
          dplyr::select(one_of(full_cols))
        i1 <- match(colnames(valuation_df), full_cols, nomatch = 0)
        colnames(valuation_df)[i1] <- target_cols[i1]
        if (is.null(agg_df)){
          agg_df <- valuation_df
        }else{
          agg_df <- agg_df %>%
          rbind.fill(valuation_df)
        }}
    }
  }
}

# Save first-pass values in csv file
write_csv(agg_df, "O:/CCAODATA/output_data/open_data/first_pass_values.csv")
      